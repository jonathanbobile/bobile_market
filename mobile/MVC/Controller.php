<?php
include_once "../mobile/libs/Mobile_Detect.php";
/**
 * This is the "base controller class". All other "real" controllers extend this class.
 * Whenever a controller is created, we also
 * 1. create a view object
 */
class Controller
{
    public $view;

    function __construct($isAsync = false)
    {
        


        // create a view object (that does nothing, but provides the view render() method)
        $this->view = new View();
        $detect = new Mobile_Detect;

        if($detect->isMobile() && !$detect->isTablet()){

        
        }
        
        $this->view->controller = strtolower(get_class($this));

    }

    /**
     * loads the model with the given name.
     * @param $model string name of the model
     */
    public function loadModel($model)
    {
        $path = MODELS_PATH . $model . '.php';

        if (file_exists($path)) {
            require_once MODELS_PATH . $model . '.php';

            return new $model();
        }
        else return 0;
    }

    public function checkSession($session){
        if(!isset($session)){
            $this->echoLogoutScript();
            exit;
        }
    }
}
