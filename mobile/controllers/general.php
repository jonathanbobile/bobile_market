<?php

class General extends Controller{

    function __construct(){
        parent::__construct();

        $this->view->scripts[] = URL . SCRIPTS_PATH . 'global.js';
    }
    
    function index(){
        $host = strtolower($_SERVER['HTTP_HOST']);
        $URL = utilityManager::file_get_contents_pt("https://api.bobile.com/api/app_market.php?action=getWhiteLabelDomain&domain=$host");
        
        Header("HTTP/1.1 301 Moved Permanently");
        Header("Location: $URL");
    }

}
