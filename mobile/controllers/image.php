<?php

class Image extends Controller {

    function __construct()
    {
        parent::__construct();
        $this->bizID = isset($_SESSION['appData']['biz_id']) ? $_SESSION['appData']['biz_id'] : 0 ;
    }

    function update($image_type)
    {
        $image_storage = imageStorage::getStorageAdapter();
        $image_name = uniqid($this->bizID);
        $image_name .= substr($_REQUEST['image_url'], strrpos($_REQUEST['image_url'], '.'));

        //list($width, $height) = getimagesize($_REQUEST['image_url']);
       
        switch($image_type){
            case "product_img":
                $thumbWidth = 240;
                $thumbHeight = 160; 
                break;
            case "app_logo":
                $thumbWidth = 400;
                $thumbHeight = 200; 
                break;
            case "content_img":   
            case "slider_img": 
                $thumbWidth = 300;
                $thumbHeight = 166; 
                break;
            case "news_img":
            case "video_img":
                $thumbWidth = 300;
                $thumbHeight = 190; 
                break;
            case "coupon_img":   
            case "program_img":
                $thumbWidth = 373;
                $thumbHeight = 257; 
                break;
            case "splash_screen":
                $thumbWidth = 200;
                $thumbHeight = 266; 
                break;
            case "biz_module_img":
            case "module_img":  
                $thumbWidth = 290;
                $thumbHeight = 290; 
                break;    
            case "push_img":  
                $thumbWidth = 280;
                $thumbHeight = 146;                
                break; 
            case "gallery_img":  
                $thumbWidth = 124;
                $thumbHeight = 124;                
                break;
            case "custom_img_back":
            case "custom_img_magic":
                $thumbWidth = 828;
                $thumbHeight = 1470; 
                break;
            default:
                $thumbWidth = 108;
                $thumbHeight = 108;
        }
        
        //$fh = fopen("debugImageSave.txt", 'a');
        // fwrite($fh, print_r($_REQUEST,true)."\n");
        // fclose($fh);
        $cropWidth = $_REQUEST['cropWidth'];
        $cropHeight = $_REQUEST['cropHeight'];
        if($image_type == "custom_img_back" || $image_type == "custom_img_magic"){
            $theme_name = isset($_REQUEST['targetFolder']) ? $_REQUEST['targetFolder'] : "custom_".$this->bizID;
            $jpg_name = $image_type == "custom_img_back" ? "new_background.jpg" : "magicmenu_background.jpg";
            //$image_url = $image_storage->uploadFile($_REQUEST['image_url'], 'templates/' . $theme_name . '/' . $jpg_name, $thumbWidth, $thumbHeight,$cropWidth,$cropHeight);

            //$fh = fopen("debugImageCustom.txt", 'a');
            // fwrite($fh, print_r($_REQUEST,true)."\n");
            // fclose($fh);
            $new_image = imageManager::resizeImage($_REQUEST['image_url'],828,1470,'templates/' . $theme_name . '/' . $jpg_name,'jpg',828,1470);
            
            
            $rand = rand(100000,9999999);
            $new_image .= '?temp='.$rand;
            echo $new_image;
            return;
        }
        else{
            $_REQUEST['image_url'] = $image_storage->uploadFile($_REQUEST['image_url'], $this->bizID . '/' . $image_name, $thumbWidth, $thumbHeight,$cropWidth,$cropHeight);
            $_REQUEST['uid'] = $this->bizID;

            $image_manager = new ImageManager();
            if($_REQUEST['row_id'] == "" && $image_type=="gallery_img"){
                $method = 'insert_' . $image_type;
            }
            else{
                $method = 'update_' . $image_type;
            }
            
            
            
            $image = $image_manager->$method($_REQUEST);

            

            if($image_type == 'gallery_img' && $_REQUEST['row_id'] == ""){
                $modManager = new moduleManager();
                $this->view->element = $image;
                
                $this->view->modMeta = $modManager->getModMetaData($_REQUEST['mod_id']);
                if($_REQUEST['mod_id'] == 4){
                    $modManager->updateGalleryChildCount($image['md_parent'],1,'photos');
                    $modManager->updateGalleryFirstImage($image['md_parent']);
                }
                $this->view->render('editor/elements/elem_gallery_photo',1);
            }
            else{
                $thumb = str_replace("storage.googleapis.com/paptap","storage.googleapis.com/paptap-thumbs", $image);
                echo $thumb;
            }
            generalManager::needUpdateApp();
        }
    }

    function updatePDF(){
        $file = $_POST['file'];
        $name = $_POST['filename'];
        $rowId = $_POST['rowid'];
        $modId = $_POST['modid'];

        $moduleManager = new moduleManager();
        $values = array();
        $values['md_pic'] = $file;
        $values['md_desc'] = $name;
        $res['result'] = $moduleManager->updateModDataRow($modId,$rowId,$values);
        $res['href'] = $file;
        
        header('Content-type: application/json');
        echo json_encode($res);
    }

    function paptap_gallery($innerPath = ""){

        $this->view->scripts[] = URL . SCRIPTS_PATH . 'gallery_images.js';
        $this->editorManager = $this->loadModel('editorManager');
        $this->view->categories = $this->editorManager->getCategoriesAggregated();
        $this->view->subCategories = $this->editorManager->getSubCategories(0,true);
        $this->view->destPath = str_replace('|','/',$innerPath);
        $this->view->destPath = str_replace('*_*',' ',$this->view->destPath);
         //$fh = fopen("debugImage.txt", 'a');
         //fwrite($fh, $innerPath."\n");
         //fwrite($fh, $this->view->destPath."\n");
         //fclose($fh);
        $this->view->render('global/image_gallery',1);


    }

    function load_sub_cat_images($subId){

        $image_storage = imageStorage::getStorageAdapter();
        $this->view->imagesToShow = $image_storage->getLibraryFiles("gallery-images/$subId/");
        $this->view->render('global/image_collection',1);
    }

    function copy_image(){
        try{
            $from = $_REQUEST["origin"];
            $folder = $_REQUEST["destFolder"];
            $this->create_originImg($from,$_SESSION["appData"]["biz_id"],$folder);
            echo 1;
        }
        catch(Exception $ex){
            echo 'failed: '.$ex->getMessage();
        }
    }

    function create_originImg($imgURL,$bizId,$folder=''){

        $imgURL = str_replace('https://storage.googleapis.com/paptap/', '', $imgURL);
        if ($folder != "") $folder = "/".$folder;
        
        $this->image_storage = imageStorage::getStorageAdapter();        
        $this->image_storage->copyFile($imgURL, $bizId . '/original' . $folder . strrchr($imgURL, '/'));
    }

    function getAviaryEncrypt(){
        $tstamp = time();
        $salt = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
        $signature = sha1("1bdade9c162543efba868671e6820fe8"."e3881559-f672-40ed-b315-2f8cc074a3b6".$tstamp.$salt);
        $encrypt = array(
            'timestamp' => $tstamp,
            'salt' => $salt,
            'encryptionMethod' => 'sha1',
            'signature' => $signature,
            );


        header('Content-type: application/json');
        echo json_encode($encrypt);
        
    }

    function paint_image($imageFolder,$imageName,$color){

        $im_url = URL."img/$imageFolder/$imageName.png";
        $color = "#".$color;

        $im_src = imagecreatefrompng($im_url);
        imagealphablending($im_src, true);
        imagesavealpha($im_src, true);

        $width = imagesx($im_src);
        $height = imagesy($im_src);
        $rgb = explode(",",utilityManager::hex2rgb($color));

        $im_dst = $im_src;

        for( $x=0; $x<$width; $x++ ) {
            for( $y=0; $y<$height; $y++ ) {

                $alpha = ((imagecolorat( $im_src, $x, $y ) >> 24) & 0x7F);

                $col = imagecolorallocatealpha( $im_dst,
                    $rgb[0],
                    $rgb[1],
                    $rgb[2],
                    $alpha
                    );

                imagesetpixel( $im_dst, $x, $y, $col );
            }
        }

        header('Content-type: image/png;');
        imagepng($im_dst);
    }
}




?>