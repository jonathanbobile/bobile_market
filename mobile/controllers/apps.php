<?php

class Apps extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->view->scripts[] = URL . SCRIPTS_PATH . 'global.js';
        $this->view->lang = utilityManager::loadLanguage("en");
        $this->view->langCode = "en";
    }

    public function index(){
        Header("HTTP/1.1 301 Moved Permanently");
        Header("Location: " . URL . "apps/home");
    }

    public function home(){

        $response = utilityManager::file_get_contents_pt("https://api.bobile.com/api/app_market.php?action=getMarketFeatured&marketId=0"); 
        $this->view->data = json_decode($response);

        $this->view->render('apps/home');

    }

    function categories(){

    }

    function search($term=""){

        $response = utilityManager::file_get_contents_pt("https://api.bobile.com/api/app_market.php?action=search&marketId=0&qs=$term");
    
        $this->view->data = json_decode($response);

        $this->view->render('apps/search_result');
    }
}
?>
