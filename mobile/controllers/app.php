<?php

class App extends Controller{

    function __construct(){
        parent::__construct();

        $this->view->scripts[] = URL . SCRIPTS_PATH . 'global.js';
        $this->view->lang = utilityManager::loadLanguage("en");
        $this->view->langCode = "en";
    }
    
    function index(){
        Header("HTTP/1.1 301 Moved Permanently");
        Header("Location: " . URL . "app/overview");
    }

    function overview($biz_id){

        $bizId = utilityManager::decryptIt($biz_id,true);

        $host = strtolower($_SERVER['HTTP_HOST']);

        $response = utilityManager::file_get_contents_pt("https://api.bobile.com/api/app_market.php?action=bizInfoInner&bizid=$bizId&marketId=0&domain=$host"); 
        $this->view->data = json_decode($response);
        $this->view->bizId = $biz_id;
        $this->view->ipaId = $bizId + 14000000;
        $this->view->hasRating = isset($_COOKIE["rating"]) ? true : false;

        $lang = isset($this->view->data->accountData->ac_lang) && $this->view->data->accountData->ac_lang != "" ? $this->view->data->accountData->ac_lang : "en";
        $this->view->lang = utilityManager::loadLanguage($lang);
        
        $this->view->langCode = $lang;

        $detect = new Mobile_Detect;
        if($detect->is("iOS")){
            $this->view->isIOS = true;
        }else{
            $this->view->isIOS = false;
        }

        $this->view->isDevice = $detect->isMobile() || $detect->isTablet();

        $this->view->render('app/index');
    }

    function privacy($biz_id,$from_mobile=""){

        $bizId = utilityManager::decryptIt($biz_id,true);

        $host = strtolower($_SERVER['HTTP_HOST']);

        $response = utilityManager::file_get_contents_pt("https://api.bobile.com/api/app_market.php?action=bizInfoInner&bizid=$bizId&marketId=0&domain=$host"); 

        $lang = isset($this->view->data->accountData->ac_lang) && $this->view->data->accountData->ac_lang != "" ? $this->view->data->accountData->ac_lang : "en";
        $this->view->lang = utilityManager::loadLanguage($lang);
        $this->view->langCode = $lang;

        $this->view->data = json_decode($response);
        $this->view->bizId = $biz_id;
        $this->view->from_mobile = $from_mobile;
        $this->view->render('app/privacy');
    }

    function getReviews(){

        $biz_id = utilityManager::decryptIt($_REQUEST["bizId"],true);
        $limitStart = $_REQUEST["limitStart"];

        $response = utilityManager::file_get_contents_pt("https://api.bobile.com/api/app_market.php?action=getRating&bizid=$biz_id&marketId=0&limitStart=$limitStart"); 
        $this->view->data = json_decode($response);
        
        $this->view->render('app/reviews');
    }

    function setRating(){

        $biz_id = utilityManager::decryptIt($_REQUEST["bizId"],true);
        $score = $_REQUEST["score"];
        $visitorsName = urlencode($_REQUEST["visitorsName"]);
        $visitorsComments = urlencode($_REQUEST["visitorsComments"]);

        utilityManager::file_get_contents_pt("https://api.bobile.com/api/app_market.php?action=setRating&bizid=$biz_id&marketId=0&rating=$score&name=$visitorsName&review=$visitorsComments"); 
        echo "ok";
    }
}
