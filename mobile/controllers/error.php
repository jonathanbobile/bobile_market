<?php

class Error extends Controller
{
    function __construct()
    {
        parent::__construct(); 
    }
    
    function index()
    {
        header("HTTP/1.0 404 Not Found");
        $this->view->render('error/404', 0, 0);
    }    
}
