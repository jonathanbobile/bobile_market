<?php

/**
 * An adapter class for google storage
 *
 *
 * @version 1.0
 * @author Alex
 */

require_once 'imageStorageInterface.php';

class ImageStorageGoogle implements ImageStorageInterface {

    private $_login;
    private $_key;
    private $_scope;
    private $_project;
    private $_storage;
    private $_api;

    function __construct()
    {
        $this->_login = 'paptap@tough-study-125310.iam.gserviceaccount.com';
        $this->_key = ROOT_PATH.'libs/google-api-client/PapTap Master-55c82cab809a.p12';
        $this->_scope = 'https://www.googleapis.com/auth/devstorage.read_write';
        $this->_project = 'tough-study-125310';
        $this->_storage = array(
                    'url' => 'https://storage.googleapis.com/',
                    'prefix' => '');

        require_once 'google-api-client/src/Google/autoload.php';

        $credentials = new Google_Auth_AssertionCredentials($this->_login, $this->_scope, utilityManager::file_get_contents_pt($this->_key));

        $client = new Google_Client();
        $client->setAssertionCredentials($credentials);
        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion();
        }

        # Starting Webmaster Tools Service
        $this->_api = new Google_Service_Storage($client);
    }

    public function uploadFile($file_path, $storage_path, $thumb_width = 122, $thumb_height = 91,$crop_width = 0, $crop_height = 0)
    {
        $file_path = (!utilityManager::startsWith($file_path,"http") && !utilityManager::startsWith($file_path,"/")) ? "http://$file_path" : $file_path;
        $file_content = utilityManager::file_get_contents_pt($file_path);
        return $this->uploadFileData($file_content, $storage_path, $thumb_width, $thumb_height, $file_path,$crop_width,$crop_height);
    }

    public function directUpload($file_path, $storage_path,$bucket = 'paptap'){
        $file_path = (!utilityManager::startsWith($file_path,"http") && !utilityManager::startsWith($file_path,"/")) ? "http://$file_path" : $file_path;
        $file_content = utilityManager::file_get_contents_pt($file_path);

        $object = new Google_Service_Storage_StorageObject();
        $object->setName($storage_path);

        if(substr(strstr($storage_path, '.'), 1) == 'pdf'){
            $response = $this->_api->objects->insert($bucket,
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'application/' . substr(strstr($storage_path, '.'), 1),
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));

        }
        else{
            $response = $this->_api->objects->insert($bucket,
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'image/' . substr(strstr($storage_path, '.'), 1),
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));
        }

        return "https://storage.googleapis.com/$bucket/".$response["name"];
    }

    public function uploadFileData($file_content, $storage_path, $thumb_width = 122, $thumb_height = 91, $file_path = '',$crop_width = 0, $crop_height = 0)
    {
        if($crop_width == 0 || $crop_height == 0){
            $crop_width = $thumb_width;
            $crop_height = $thumb_height;
        }
        $object = new Google_Service_Storage_StorageObject();
        $object->setName($storage_path);

         //$fh = fopen("debugImageSave.txt", 'a');
         //fwrite($fh, $file_path."\n");
         //fwrite($fh, $storage_path."\n");

         //fclose($fh);
        if($crop_width != 1 && $crop_height != 1 && substr(strstr($storage_path, '.'), 1) != 'pdf'){
            //Crop image before upload
            require_once 'imageManager.php';
            $extension = substr(strstr($storage_path, '.'), 1);
            $file_data = @imagecreatefromstring($file_content);
            $cropped = imageManager::createThumb($file_data, $extension, $crop_width, $crop_height);
            ob_start();
            imagepng($cropped);
            $file_content =  ob_get_contents();
            ob_end_clean();
            //end crop
        }



        if(substr(strstr($storage_path, '.'), 1) == 'pdf'){
            $response = $this->_api->objects->insert('paptap',
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'application/' . substr(strstr($storage_path, '.'), 1),
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));

        }
        else{
            $response = $this->_api->objects->insert('paptap',
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'image/' . substr(strstr($storage_path, '.'), 1),
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));
        }

        if (substr($file_path, strrpos($file_path, '/')) != '/empty_file') {

            $file_data = @imagecreatefromstring($file_content);
            if($file_data !== false){
                $this->createAndUploadThumb($file_data, $storage_path, $thumb_width, $thumb_height);
            }
        }

        return "https://storage.googleapis.com/paptap/".$response["name"];
    }

    public function createAndUploadThumb($file_data, $storage_path, $thumb_width, $thumb_height)
    {
        $extension = substr(strstr($storage_path, '.'), 1);
        require_once 'imageManager.php';
        $thumb = imageManager::createThumb($file_data, $extension, $thumb_width, $thumb_height);
        ob_start();
        imagepng($thumb);
        $file_content =  ob_get_contents();
        ob_end_clean();

        $object = new Google_Service_Storage_StorageObject();
        $object->setName($storage_path);

        $result = $this->_api->objects->insert('paptap-thumbs',
                                               $object,
                                               array('uploadType' => 'media',
                                                     'mimeType' => 'image/' . substr(strstr($storage_path, '.'), 1),
                                                     'data' => $file_content,
                                                     'predefinedAcl' => 'publicRead'));
        return $result;
    }

    public function deleteFiles($path)
    {
       $res = $this->_api->objects->listObjects('paptap', array('prefix' => $path));

        if (isset($res['items'])) {
            foreach($res['items'] as $item) {
                $this->deleteFile($item['name']);
            }
        }
    }

    public function deleteFile($path)
    {
        try{
            $this->_api->objects->delete('paptap', $path);
        }
        catch(Exception $e){}

        try{
            $this->_api->objects->delete('paptap-thumbs', $path);
        }
        catch(Exception $e){}
    }

    public function createFolder($folder,$bucket = 'paptap')
    {
        $folder = rtrim($folder, '/') . '/'; // adding trailing slash if not exists. If object has a trailing slash it is added as folder
        if (!$this->exists($folder,$bucket)) {
            $object = new Google_Service_Storage_StorageObject();
            $object->setName($folder);
            $this->_api->objects->insert($bucket,
                                               $object,
                                               array('uploadType' => 'media',
                                                     'data' => '',
                                                     'predefinedAcl' => 'publicRead'));
        } else {
            echo 'Folder exists';
        }
        return '';
    }

    public function getFilesInFolder($biz_id, $folder = '', $pdfOnly = false)
    {

        $prefix = $biz_id . '/' . $folder;

        
        $objs = $this->_api->objects->listObjects('paptap', array('maxResults' => 10000, 'prefix' => $prefix));
       
        $files = array();
        $folders = array();
        if(!isset($biz_id) || (is_int($biz_id) && $biz_id <= 0)){
           
            return $files;
        }
        if ($folder) {
            $folders[] = array('file' => '..', 'date' => '', 'size' => '', 'extension' => 'dir');
        }
       
        
       
        
        foreach($objs['items'] as $obj) {
            
            
            if ($obj['name'] == $prefix) { continue; }
            $filename = str_replace($prefix, '', $obj['name']);
            
            if (strpos($filename, '/') !== false) {
                $path_parts = explode('/', $filename);
                $folders[$path_parts[0]] = array('file' => $path_parts[0], 'date' => $obj['updated'], 'size' =>  $obj['size'], 'extension' => 'dir');
            } else {
                $file_parts = explode('.', $filename);
                if((!$pdfOnly && $file_parts[1] != 'pdf') || ($pdfOnly && $file_parts[1] == 'pdf')){
                    $files[] = array('file' => 'https://storage.googleapis.com/paptap/' . $obj['name'], 'date' => $obj['updated'], 'size' => $obj['size'], 'extension' => $file_parts[1], 'name' => $filename);
                }
            }
        }
        $files = array_merge(array_values($folders), $files);
        return $files;
    }

    public function getLibraryFiles($folder = '')
    {
        $objs = $this->_api->objects->listObjects('paptap', array('maxResults' => 10000, 'prefix' => $folder));
        
        $files = array();
        $folders = array();

        if ($folder) {
            $folders[] = array('file' => '..', 'date' => '', 'size' => '', 'extension' => 'dir');
        }
               
        foreach($objs['items'] as $obj) {

            if ($obj['name'] == $folder) { continue; }
            $filename = str_replace($folder, '', $obj['name']);
            $file_parts = explode('.', $filename);
            $files[] = array('file' => 'https://storage.googleapis.com/paptap/' . $obj['name'], 'date' => $obj['updated'], 'size' => $obj['size'], 'extension' => $file_parts[1], 'name' => $filename);
        }

        return $files;
    }
    
    public function getFolderContents($folder,$bucket = 'paptap',$showDirs = false){
        $objs = $this->_api->objects->listObjects($bucket, array('maxResults' => 10000, 'prefix' => $folder));

        $folders = array();
        $files = array();

        foreach($objs['items'] as $obj) {


            if ($obj['name'] == $folder) { continue; }

            $filename = str_replace($folder.'/', '', $obj['name']);

            if (strpos($filename, '/') !== false) {
                if($showDirs){
                    $path_parts = explode('/', $filename);
                    $folders[] = array('file' => $path_parts[0], 'date' => $obj['updated'], 'size' =>  $obj['size'], 'extension' => 'dir');
                }
            } else {
                $file_parts = explode('.', $filename);
                if(isset($file_parts[1])){
                    $files[] = array('file' => 'https://storage.googleapis.com/'.$bucket.'/' . $obj['name'], 'date' => $obj['updated'], 'size' => $obj['size'], 'extension' => $file_parts[1], 'name' => $filename);
                }

            }
        }

        $files = array_merge($folders,$files);
        return $files;
    }

    public function getFileCount($folder,$bucket = 'paptap'){
        $objs = $this->_api->objects->listObjects($bucket, array('maxResults' => 10000, 'prefix' => $folder));

        $result = array();

        $result['root'] = 0;

        foreach($objs['items'] as $obj) {


            if ($obj['name'] == $folder) { continue; }

            $filename = str_replace($folder.'/', '', $obj['name']);

            if (strpos($filename, '/') !== false) {
                $path_parts = explode('/', $filename);
                if(!isset($result[$path_parts[0]])){
                    $result[$path_parts[0]] = 0;
                }

                $result[$path_parts[0]] += 1;

            } else {

                $result['root'] += 1;
            }
        }

        return $result;
    }

    public function copyFiles($source_path, $dest_path)
    {
        // Checking if this function is used for renaming or moving files
        $rename = false;
        if ((strpos($source_path, '.') !== false && strpos($dest_path, '.') !== false) || (strpos($source_path, '.') === false && !$this->exists($dest_path))) {
            if ($this->exists($dest_path)) {
                die('File exists');
            }
            $rename = true;
        }

        $res = $this->_api->objects->listObjects('paptap', array('prefix' => $source_path));
        $parent_folders = str_replace(strrchr(trim($source_path, '/'), '/'), '', $source_path);

        if (isset($res['items'])) {
            foreach($res['items'] as $item) {
                if ($rename) {
                    $dest_name = str_replace($source_path, $dest_path, $item['name']);
                } else {
                    $dest_name = str_replace('//' , '/', $dest_path . str_replace($parent_folders, '', $item['name']));
                }
                $this->copyFile($item['name'], $dest_name);
            }
        }
        return 1;
    }

    public function copyFile($source_path, $dest_path)
    {
        if ($source_path == $dest_path) {
            die('Illegal copy');
        }

        $object = new Google_Service_Storage_StorageObject();
        try
        {

            $this->_api->objects->copy('paptap', $source_path, 'paptap', $dest_path, $object, array('destinationPredefinedAcl' => 'publicRead'));
            $this->_api->objects->copy('paptap-thumbs', $source_path, 'paptap-thumbs', $dest_path, $object,array('destinationPredefinedAcl' => 'publicRead'));
        }
        catch(Google_Service_Exception $e) {
            // some error occured, but we proceed with other files

        }
        return $dest_path;
    }

    public function moveFiles($source_path, $dest_path)
    {
        $this->copyFiles($source_path, $dest_path);
        if (strpos($source_path, '.') !== false) {
            $this->deleteFiles($source_path);
        } else {
            $this->deleteFiles($source_path);
        }
        return 1;
    }

    public function exists($path,$bucket = 'paptap')
    {
        try
        {
            $prefix = rtrim($path, '/');
            $res = $this->_api->objects->listObjects($bucket, array('maxResults' => 10000, 'prefix' => $prefix));
           
            return count($res['modelData']) > 0;

        }
        catch(Google_Service_Exception $e) {
            if (strpos($e->getMessage(), 'Forbidden') === false) { // if the error is 'forbidden' it means the object exists, otherwise it is 'not found'
                return false;
            } else {
                return true;
            }
        }

    }

    public function uploadDocument($file_content, $storage_path,$mimeType)
    {
        $object = new Google_Service_Storage_StorageObject();
        $object->setName($storage_path);

        $response = $this->_api->objects->insert('paptap',
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'application/'.$mimeType,
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));


        return "https://storage.googleapis.com/paptap/".$response["name"];
    }
}

?>