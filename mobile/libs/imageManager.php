<?php


class imageManager
{
    function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.');
        }
        $this->bizID = isset($_SESSION['appData']['biz_id']) ? $_SESSION['appData']['biz_id'] : 0;
    }

    public static function createThumb($src_r, $extension, $th_width = 122, $th_height = 91)
    {
        $instance = new self();
        $src_width = imagesx($src_r);
        $src_height = imagesy($src_r);

        if($src_width < $th_width && $src_height < $th_height){
            $th_width = $src_width;
            $th_height = $src_height;
        }


        if ($src_width / $src_height > $th_width / $th_height) {
            $width = round($src_height * $th_width / $th_height);
            $height = $src_height;
            $x = ($src_width - $width) / 2;
            $y = 0;
        } else {
            $height = round($src_width * $th_height / $th_width);
            $width = $src_width;
            $x = 0;
            $y = ($src_height - $height) / 2;
        }

        $dst_r = $instance->getBlankImage($th_width, $th_height, $extension);

        imagecopyresampled($dst_r, $src_r, 0, 0, $x, $y, $th_width, $th_height, $width, $height);

        return $dst_r;
    }


    public static function getBlankImage($w, $h, $extension)
    {
        $dst_r = ImageCreateTrueColor($w, $h);
        switch (strtolower($extension))
        {
            case 'png':
                $background = imagecolorallocate($dst_r, 255, 255, 255);
                imagecolortransparent($dst_r, $background);
                imagealphablending($dst_r, false);
                imagesavealpha($dst_r, true);
                break;
            case 'gif':
                $background = imagecolorallocate($dst_r, 255, 255, 255);
                imagecolortransparent($dst_r, $background);
                break;
        }
        return $dst_r;
    }


    public static function createSplash($bizID,$app_name,$templateID) {
        $instance = new self();

        $themeRow = $instance->db->getRow("SELECT * FROM tbl_template WHERE tmpl_id = $templateID");
        $originalImage = imagecreatefromjpeg(IMAGE_STORAGE_PATH . "templates/{$themeRow["tmpl_name"]}/magicmenu_background.jpg");

        $img_back = imagecreatetruecolor( 1536, 2048 );
        imagecopyresampled( $img_back, $originalImage, 0, 0, 0, 0, 1536, 2048, imagesx($originalImage), imagesy($originalImage) );

        imagefilter($img_back, IMG_FILTER_GRAYSCALE);

        $imgText = stripcslashes($app_name);
        $font = dirname(dirname(__FILE__))."/styles/fonts/arialbd.ttf";
        $fontsize =  $instance->getFontSize(imagesx($img_back) - 200,$font,$imgText);
        if($fontsize > 80) $fontsize=80;

        $bbox = imagettfbbox($fontsize, 0, $font, $imgText);
        $x = $bbox[0] + (imagesx($img_back) / 2) - ($bbox[4] / 2);
        $y = 350;
        $textColor = $instance->hexColorAlloc($img_back, $themeRow["tmpl_color_6"]);
        $borderColor = $instance->hexColorAlloc($img_back, $themeRow["tmpl_color_1"]);


        $instance->imagettfstroketext($img_back,$fontsize,0, $x, $y,$textColor,$borderColor,$font,$imgText,3);

        ob_start();
        imagepng($img_back);
        $image_string = ob_get_contents();
        ob_end_clean();

        $image_storage = imageStorage::getStorageAdapter();
        $amazon_path = "$bizID/" . uniqid($bizID) . '.png';
        $image = $image_storage->uploadFileData($image_string, $amazon_path,200,266,"",1536,2048);

        $data = array('image_url' => $image, 'uid' => $bizID);
        return $instance->update_splash_screen($data);
    }

    public static function resizeImage($image,$width,$height,$dest,$type = 'jpg',$thumbWidth = 0, $thumbHeight = 0){
       
        $img_back = imagecreatetruecolor( $width, $height );
        
        if(strpos($image, '.jpg') !== false){
            $originalImage = imagecreatefromjpeg($image);
        }
        if(strpos($image, '.png') !== false){
            $originalImage = imagecreatefrompng($image);
        }
        imagecopyresampled( $img_back, $originalImage, 0, 0, 0, 0, $width, $height, imagesx($originalImage), imagesy($originalImage) );
        

        ob_start();
        if($type == 'jpg'){
            imagejpeg($img_back);
        }
        else{
            imagepng($img_back);
        }
        $image_string = ob_get_contents();
        ob_end_clean();

         $image_storage = imageStorage::getStorageAdapter();
         
         $image = $image_storage->uploadFileData($image_string, $dest,$width,$height);
         return $image;
    }


    public static function createMarketIcon($icon)
    {
        $instance = new self();
        $image_storage = imageStorage::getStorageAdapter();
        $amazon_path = $_SESSION['id'] . '/' . uniqid($this->bizID) . '.png';
        $image = $image_storage->uploadFile("../../graphics/icons/big/" . $icon . ".png", $amazon_path);

        $data = array('image_url' => $image, 'uid' => $_SESSION['id']);
        return $instance->update_market_icon($data);
    }


    public static function createLogo($bizID,$shortName,$templateID)
    {
        $instance = new self();

        $themeRow = $instance->db->getRow("SELECT * FROM tbl_template WHERE tmpl_id = $templateID");

        //$originalImage = URL . "img/emptyLogo.png";
        $colors = array();
        $colors[0] = $themeRow["tmpl_color1_freestyle"];
        $colors[1] = $themeRow["tmpl_color1_freestyle"];
        $colors[2] = $themeRow["tmpl_color4_freestyle"];
        $colors[3] = $themeRow["tmpl_color4_freestyle"];

        $im = $instance->gradient(500,250,$colors);

        imagesavealpha($im, true); // important to keep the png's transparency
        $digit = stripcslashes($shortName);
        $font = dirname(dirname(__FILE__))."/styles/fonts/arialbd.ttf";
        $fontsize =  $instance->getFontSize(imagesx($im) - 50,$font,$digit);
        if($fontsize > 60) $fontsize=60;
        $outputImage = str_replace(".","",microtime(true)).".png";

        $bbox = imagettfbbox($fontsize, 0, $font, $digit);
        $x = $bbox[0] + (imagesx($im) / 2) - ($bbox[4] / 2);
        $y = (imagesy($im) / 2) - ($bbox[5] / 2);

        $textColor = $instance->hexColorAlloc($im, $themeRow["tmpl_color2_freestyle"]);
        imagettftext($im, $fontsize, 0, $x, $y, $textColor, $font, $digit);

        $newImgFile  = dirname(dirname(__FILE__))."/img/bizImages/".$outputImage;
        imagepng($im, $newImgFile, 9);  //to file
        imagedestroy($im);
        return $newImgFile;
    }

    public static function createIcon($bizID,$shortName,$templateID)
    {
        $instance = new self();
        $themeRow = $instance->db->getRow("SELECT * FROM tbl_template WHERE tmpl_id = $templateID");

        $instance = new self();
        //$originalImage = URL . "img/emptyIcon.png";
        //$im = imagecreatefrompng($originalImage);
        $colors = array();
        $colors[0] = $themeRow["tmpl_color4_freestyle"];
        $colors[1] = $themeRow["tmpl_color4_freestyle"];
        $colors[2] = $themeRow["tmpl_color1_freestyle"];
        $colors[3] = $themeRow["tmpl_color1_freestyle"];

        $im = $instance->gradient(1024,1024,$colors);
        $digit = stripcslashes($shortName);
        $nameArray = explode(" ",$digit);
        $digit = "";
        foreach ($nameArray as $oneWord)
        {
            if($oneWord != ""){
        	    $digit .= strtoupper($oneWord[0]);
            }
        }

        $font = dirname(dirname(__FILE__))."/styles/fonts/arialbd.ttf";
        $fontsize =  $instance->getFontSize(imagesx($im) - 50,$font,$digit);
        if($fontsize > 400) $fontsize=400;
        $outputImage = str_replace(".","",microtime(true)).".png";

        $bbox = imagettfbbox($fontsize, 0, $font, $digit);
        $x = $bbox[0] + (imagesx($im) / 2) - ($bbox[4] / 2);
        $y = (imagesy($im) / 2) - ($bbox[5] / 2);

        $textColor = $instance->hexColorAlloc($im, $themeRow["tmpl_color2_freestyle"]);
        //$lime = imagecolorallocate($im, 0, 0, 0); // font color

        imagettftext($im, $fontsize, 0, $x, $y, $textColor, $font, $digit);

        $newImgFile  = dirname(dirname(__FILE__))."/img/bizImages/".$outputImage;
        imagepng($im, $newImgFile, 9);  //to file
        imagedestroy($im);
        return $newImgFile;
    }


    public static function update_app_icon($data)
    {
        $instance = new self();
        $instance->db->execute("UPDATE tbl_biz SET biz_icon = '" . $data['image_url'] . "', biz_submit_icon = '" . $data['image_url'] . "' WHERE biz_id = " . intval($data['uid']));
        $_SESSION["appData"]["biz_icon"] = $data['image_url'];
        $_SESSION["appData"]["biz_submit_icon"] = $data['image_url'];
        return $data['image_url'];
    }


    public function update_market_icon($data)
    {
        $this->db->execute("UPDATE tbl_biz SET biz_icon = '" . $data['image_url'] . "', biz_submit_icon = '" . $data['image_url'] . "' WHERE biz_id = " . intval($data['uid']));
        $_SESSION["appData"]["biz_icon"] = $data['image_url'];
        $_SESSION["appData"]["biz_submit_icon"] = $data['image_url'];
        return $data['image_url'];
    }


    public static function update_app_logo($data, $autoGenerated=false)
    {
        $instance = new self();
        $addLogoToUpdate = ($autoGenerated) ? ",biz_custom_logo=0" : "";

        $instance->db->execute("UPDATE tbl_biz SET biz_logo = '{$data['image_url']}' $addLogoToUpdate WHERE biz_id = " . intval($data['uid']));
        $_SESSION["appData"]["biz_logo"] = $data['image_url'];

        $instance->db->execute("UPDATE tbl_mod_data0 SET md_pic = '" . $data['image_url'] . "' WHERE md_mod_id = 0 AND md_belong_to_structure=0 AND md_biz_id = " . intval($data['uid']));
        return $data['image_url'];
    }


    public function update_splash_screen($data)
    {
        $this->db->execute("UPDATE tbl_biz SET biz_submit_splash = '{$data['image_url']}' WHERE biz_id = {$data['uid']}");
        $_SESSION["appData"]["biz_submit_splash"] = $data['image_url'];
        return $data['image_url'];
    }


    public function update_module_img($data)
    {
        $this->db->execute("UPDATE tbl_biz_mod SET biz_mod_mod_pic = '" . $data['image_url'] . "' WHERE biz_mod_biz_id = " . intval($data['uid']) . " AND biz_mod_mod_id = " . intval($data['mod_id']));
        $_SESSION["moduleData"][$data['mod_id']]["biz_mod_mod_pic"] = $data['image_url'];
        return $data['image_url'];
    }

    public function update_biz_module_img($data){
        //$data['mod_id'] - it is now biz_mod_id
        $this->db->execute("UPDATE tbl_biz_mod SET biz_mod_mod_pic = '" . $data['image_url'] . "' WHERE biz_mod_biz_id = " . intval($data['uid']) . " AND biz_mod_id = " . intval($data['mod_id']));
        $sql = "SELECT biz_mod_mod_id FROM tbl_biz_mod WHERE biz_mod_id = " . intval($data['mod_id']). " AND biz_mod_biz_id = " . intval($data['uid']);

        $modid = $this->db->getVal($sql);
        if(isset($_SESSION["moduleData"][$modid])){
            $_SESSION["moduleData"][$modid]["biz_mod_mod_pic"] = $data['image_url'];
        }
        return $data['image_url'];
    }


    public function update_content_img($data)
    {
        $row = $this->db->getRow("SELECT * FROM tbl_mod_data{$data['mod_id']}  WHERE md_biz_id  = ".intval($data['uid'])." AND md_row_id = " . intval($data['row_id']));
        if($row['md_parent'] != 0 || $data['mod_id'] == 1){
            $this->db->execute("UPDATE tbl_mod_data" . $data['mod_id'] . " SET md_pic = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        }
       
        return $data['image_url'];
    }

    public function insert_gallery_img($data)
    {
        $sql = "SELECT IFNULL(MAX(md_index) + 1,1) FROM tbl_mod_data{$data['mod_id']} WHERE md_biz_id={$this->bizID}
                        AND md_mod_id= {$data['mod_id']}
                        AND md_level_no={$data['level']}
                        AND md_parent={$data['mdidparent']}";

        $max_index = $this->db->getVal($sql);
        if($max_index == '' || $max_index == null){
            $max_index = 1;
        }
        $id = $this->db->execute("insert into tbl_mod_data" . $data['mod_id'] . " SET
                        md_biz_id={$this->bizID},
                        md_mod_id= {$data['mod_id']},
                        md_level_no={$data['level']},
                        md_parent={$data['mdidparent']},
                        md_icon='{$data['image_url']}',
                        md_index = $max_index,
                        md_element=12");

        return $this->db->getRow("SELECT * from tbl_mod_data{$data['mod_id']},tbl_mod_elements
                                    where me_id=md_element
                                    and md_biz_id={$_SESSION["appData"]["biz_id"]}
                                    and md_mod_id={$data['mod_id']}
                                    and md_row_id = $id
                                    order by md_index");
    }

    public function update_gallery_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data" . $data['mod_id'] . " SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));

        return $data['image_url'];
    }


    public function update_slider_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data" . $data['mod_id'] . " SET md_pic" . $data['img_index'] . " = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }


    public function update_list_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data" . $data['mod_id'] . " SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }


    public function update_news_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data3 SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }

    public function update_video_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data24 SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }

    public function update_coupon_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data26 SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }

    public function update_program_img($data)
    {
        //the image saved on program save
        return $data['image_url'];
    }

    public function update_product_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data9 SET md_icon = '" . addslashes($data['image_url']) . "' WHERE md_row_id = " . intval($data['row_id']));

        return $data['image_url'];
    }


    public function update_employee_img($data)
    {
        $this->db->execute("UPDATE tbl_employee SET be_pic = '" . $data['image_url'] . "' WHERE be_id = " . intval($data['be_id']) . " AND be_biz_id = " . intval($data['uid']));

        return $data['image_url'];
    }

    public function update_push_img($data)
    {
        $this->db->execute("UPDATE tbl_push SET pu_img = '" . $data['image_url'] . "' WHERE pu_id = " . intval($data['pu_id']) . " AND pu_biz_id = " . intval($data['uid']));

        return $data['image_url'];
    }

    public function multiUploadFiles($files,$data)
    {
        $image_storage = imageStorage::getStorageAdapter();

        $jsona = json_decode($data);
        $modID = $jsona->mid;
        $level = $jsona->level;
        $mdidparent = $jsona->mdidparent;
        $elid = $jsona->elid;
        $added = "";
        foreach($files['files']['tmp_name'] as $key => $file) {

            $file_content = utilityManager::file_get_contents_pt($file);
            $image_name = uniqid($this->bizID);
            $image_name .= substr($files['files']['name'][$key], strrpos($files['files']['name'][$key], '.'));
            $image_url = $image_storage->uploadFileData($file_content, $this->bizID . '/' . $image_name,124,124,'',1,1);
            $image_storage->uploadFile($file, $this->bizID . '/original/' . $image_name,124,124,1,1);

            $sql = "SELECT IFNULL(MAX(md_index) + 1,1) FROM tbl_mod_data$modID WHERE md_biz_id={$this->bizID}
                        AND md_mod_id= $modID
                        AND md_level_no={$level}
                        AND md_parent={$mdidparent}";

            $max_index = $this->db->getVal($sql);
            if($max_index == '' || $max_index == null){
                $max_index = 1;
            }
            $sql = "INSERT INTO tbl_mod_data$modID
                    SET md_biz_id={$this->bizID},
                        md_mod_id= $modID,
                        md_level_no={$level},
                        md_parent={$mdidparent},
                        md_index=$max_index,
                        md_icon='$image_url',
                        md_element={$elid}";

           

            $row_id = $this->db->execute($sql);

            if($added != ""){
                $added .= ",";
            }
            $added .= $row_id;
        }

        return $added;
    }

    public static function customUploadFile($bizID,$imgURL,$width=0,$height=0,$format="png",$fileName="",$crop_width = 0, $crop_height = 0)
    {
        $image_storage = imageStorage::getStorageAdapter();
        $amazon_path = ($fileName == "") ? "$bizID/". uniqid($bizID) . ".$format" : "$bizID/$fileName";

        if($width != 0){
            return $image_storage->uploadFile($imgURL, $amazon_path, $width, $height,$crop_width,$crop_height);
        }
        else{
            return $image_storage->uploadFile($imgURL, $amazon_path);
        }
    }


    public function getFontSize($targetWidth,$font,$digit)
    {
        $fontsize = 27;
        $bbox = imagettfbbox($fontsize, 0, $font, $digit);
        $scale = $targetWidth / $bbox[4];
        $fontSize = $fontsize * $scale;
        return floor($fontSize);
    }

    public function hexColorAlloc($im,$hex){
        $hex = ltrim($hex,'#');
        $a = hexdec(substr($hex,0,2));
        $b = hexdec(substr($hex,2,2));
        $c = hexdec(substr($hex,4,2));
        return imagecolorallocate($im, $a, $b, $c);
    }

    public function imagettfstroketext(&$image, $fontsize, $angle, $x, $y, &$textcolor, &$strokecolor, $fontfile, $text, $px) {
        for($c1 = ($x-abs($px)); $c1 <= ($x+abs($px)); $c1++){

            for($c2 = ($y-abs($px)); $c2 <= ($y+abs($px)); $c2++){
                imagettftext($image, $fontsize, $angle, $c1, $c2, $strokecolor, $fontfile, $text);
            }
        }
        return imagettftext($image, $fontsize, $angle, $x, $y, $textcolor, $fontfile, $text);
    }

    public function gradient($w=100, $h=100, $c=array('#FFFFFF','#FF0000','#00FF00','#0000FF'), $hex=true) {

        /*
        Generates a gradient image

        Author: Christopher Kramer

        Parameters:
        w: width in px
        h: height in px
        c: color-array with 4 elements:
        $c[0]:   top left color
        $c[1]:   top right color
        $c[2]:   bottom left color
        $c[3]:   bottom right color

        if $hex is true (default), colors are hex-strings like '#FFFFFF' (NOT '#FFF')
        if $hex is false, a color is an array of 3 elements which are the rgb-values, e.g.:
        $c[0]=array(0,255,255);

         */

        $im=imagecreatetruecolor($w,$h);

        if($hex) {  // convert hex-values to rgb
            for($i=0;$i<=3;$i++) {
                $c[$i]=$this->hex2rgb($c[$i]);
            }
        }

        $rgb=$c[0]; // start with top left color
        for($x=0;$x<=$w;$x++) { // loop columns
            for($y=0;$y<=$h;$y++) { // loop rows
                // set pixel color
                $col=imagecolorallocate($im,$rgb[0],$rgb[1],$rgb[2]);
                imagesetpixel($im,$x-1,$y-1,$col);
                // calculate new color
                for($i=0;$i<=2;$i++) {
                    $rgb[$i]=
                      $c[0][$i]*(($w-$x)*($h-$y)/($w*$h)) +
                      $c[1][$i]*($x     *($h-$y)/($w*$h)) +
                      $c[2][$i]*(($w-$x)*$y     /($w*$h)) +
                      $c[3][$i]*($x     *$y     /($w*$h));
                }
            }
        }
        return $im;
    }

    public function hex2rgb($hex)
    {
        $rgb[0]=hexdec(substr($hex,1,2));
        $rgb[1]=hexdec(substr($hex,3,2));
        $rgb[2]=hexdec(substr($hex,5,2));
        return($rgb);
    }
}
