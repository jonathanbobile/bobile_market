<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('aws-autoloader.php');

use Aws\S3\S3Client;
use Aws\Ses\SesClient;
use Aws\ElasticBeanstalk\ElasticBeanstalkClient;

require_once('imageStorageInterface.php');

class ImageStorageAmazon implements ImageStorageInterface {
	public $s3Client=null,
			$sesClient=null,
			$ci;
	private $key, $secret, $bucket, $thumbs_bucket;

	public function __construct()
	{
        $this->key = "AKIAJFVMD4OWA4DSPTJQ";
		$this->secret = "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";
		$this->regin = "http://s3-us-west-2.amazonaws.com/";
        $this->bucket = 'paptap';
        $this->thumbs_bucket = 'paptap.thumbs';
	}


	public function initS3()
    {
		if (!$this->s3Client) $this->s3Client  = S3Client::factory(array(
				'key'    => $this->key,
				'secret' => $this->secret,
				'scheme' => 'http',
		));
	}


    // Uploading file to S3. The $file_path can be either local path or a remote path.
    public function uploadFile($file_path, $amazon_path, $thumb_width = 122, $thumb_height = 91)
    {
        $file_path = (!utilityManager::startsWith($file_path,"http") && !utilityManager::startsWith($file_path,"/")) ? "http://$file_path" : $file_path;
        $file_content = utilityManager::file_get_contents_pt($file_path);
        return $this->uploadFileData($file_content, $amazon_path, $thumb_width, $thumb_height, $file_path);
   	}
    
    public function directUpload($file_path, $storage_path){
        $file_path = (!utilityManager::startsWith($file_path,"http") && !utilityManager::startsWith($file_path,"/")) ? "http://$file_path" : $file_path;
        $file_content = utilityManager::file_get_contents_pt($file_path);
        return $this->uploadFileData($file_content, $storage_path);
    }

    // Uploading file to S3. The $file_path can be either local path or a remote path.
   	public function uploadFileData($file_content, $amazon_path, $thumb_width = 122, $thumb_height = 91, $file_path = '')
    {
   		$this->initS3();

   		$result = $this->s3Client->putObject(array(
    		'Bucket'       => $this->bucket,
    		'Key'          => $amazon_path,
    		'Body'         => $file_content,
            'ContentType'  => 'image',
    		'ACL'          => "public-read"
		));
        if (substr($file_path, strrpos($file_path, '/')) != '/empty_file') {
            //$contents = fread($file_handler, $_FILES['file']['size']);  
            $file_data = imagecreatefromstring($file_content);
            $this->createAndUploadThumb($file_data, $amazon_path, $thumb_width, $thumb_height);
        }
   		return $result['ObjectURL'];
   	}


    // Creating thumbnail using external class (imageManager). The file_data is an image resource
    public function createAndUploadThumb($file_data, $amazon_path, $thumb_width, $thumb_height)
    {
        $extension = substr(strstr($amazon_path, '.'), 1);
        require_once 'imageManager.php';
        $thumb = imageManager::createThumb($file_data, $extension, $thumb_width, $thumb_height);
        ob_start();
        imagepng($thumb);
        $contents =  ob_get_contents();
        ob_end_clean();

        $result = $this->s3Client->putObject(array(
            'Bucket' => $this->thumbs_bucket,
            'Key' => $amazon_path,
            'Body' => $contents,
            'ContentType' => 'image',
            'ACL' => "public-read"
        ));
        return $result;
    }


    // Deleting files or folders (anything that matches the path) 
    public function deleteFiles($path)
    {
        $this->initS3();
        $result = $this->s3Client->deleteMatchingObjects($this->bucket, $path);
        return $result;
    }


    // Deleting a single file
    public function deleteFile($path)
    {
        $this->initS3();
        $result = $this->s3Client->deleteObject(array(
            'Bucket' => $this->bucket,
            'Key' => $path
        ));
        return $result;
    }


    // Creating a folder by uploading an empty file with a key that contains a folder name
    public function createFolder($folder)
    {
        if (!$this->exists(strtolower($folder) . '/empty_file')) {
            $result = $this->uploadFile(PATH . 'images/empty_file', $folder . '/empty_file');
        } else {
            echo 'Folder exists';
        }
        return '';
    }

    
    // Getting all files and folders in a specific folder.
    public function getFilesInFolder($biz_id, $folder = '')
    {
        $this->initS3();
        $prefix = $biz_id . '/' . $folder;
        $objs = $this->s3Client->listObjects(array('Bucket' => 'paptap', 'Prefix' => $prefix));

        $files = array();
        $folders = array();
        if ($folder) {
            $folders[] = array('file' => '..', 'date' => '', 'size' => '', 'extension' => 'dir');
        }
        foreach($objs['Contents'] as $obj) {
            $filename = str_replace($prefix, '', $obj['Key']);
            if ($filename == 'empty_file') { continue; }
            if (strpos($filename, '/') !== false) { 
                $path_parts = explode('/', $filename);
                $folders[$path_parts[0]] = array('file' => $path_parts[0], 'date' => $obj['LastModified'], 'size' => '', 'extension' => 'dir');
            } else {
                $file_parts = explode('.', $filename);
                $files[] = array('file' => 'https://paptap.s3.amazonaws.com/' . $obj['Key'], 'date' => $obj['LastModified'], 'size' => '', 'extension' => $file_parts[1], 'name' => $filename);
            }
        }
        $files = array_merge(array_values($folders), $files); 
        return $files;
    }


    // Copying files. Can be used for copying folders as well.
    public function copyFiles($source_path, $dest_path)
    {
        // Checking if this function is used for renaming or moving files
        $rename = false;
        if ((strpos($source_path, '.') !== false && strpos($dest_path, '.') !== false) || (strpos($source_path, '.') === false && !$this->exists($dest_path))) {
            if ($this->exists($dest_path)) {
                die('File exists');
            }
            $rename = true;
        }
        
        $this->initS3();
        $objs = $this->s3Client->listObjects(array('Bucket' => 'paptap', 'Prefix' => $source_path));
        $parent_folders = str_replace(strrchr(trim($source_path, '/'), '/'), '', $source_path);
        if ($objs['Contents']) {
            foreach($objs['Contents'] as $obj) {
                if ($rename) {
                    $dest_key = str_replace($source_path, $dest_path, $obj['Key']);
                    
                } else {
                    $dest_key = str_replace('//' , '/', $dest_path . str_replace($parent_folders, '', $obj['Key']));
                    
                }
                $this->copyFile($obj['Key'], $dest_key);
            }
        }
        return 1;
    }


    // Copying files. Creating a new object with $dest_path key
    private function copyFile($source_path, $dest_path)
    {
        if ($source_path == $dest_path) {
            die('Illegal copy');
        }
        $this->initS3();
        $this->s3Client->copyObject(array(
            'Bucket'       	=> $this->bucket,
            'Key'          	=> $dest_path,
            'CopySource' 	=> $this->bucket . "/" . $source_path,
            'ACL'          	=> "public-read"
        ));

        if (substr($dest_path, strrpos($dest_path, '/')) != '/empty_file') {
            $this->s3Client->copyObject(array(
                'Bucket'       	=> $this->thumbs_bucket,
                'Key'          	=> $dest_path,
                'CopySource' 	=> $this->thumbs_bucket . "/" . $source_path,
                'ACL'          	=> "public-read"
            ));
        }
        return $dest_path;
    }

    
    // Moving or renaming files.
    public function moveFiles($source_path, $dest_path)
    {
        $this->copyFiles($source_path, $dest_path);
        $this->deleteFiles($source_path);

        return 1;
    }


    // Checking if file exists
    public function exists($file_path)
    {
        $this->initS3();
        $res = $this->s3Client->listObjects(array(
            'Bucket' => $this->bucket,
            'Prefix' => $file_path
        ));
        return isset($res['Contents']);
    }


    // This function is not used currently.
    // Helper function. Recursively building a files tree.
    private function buildTree($files, $path)
    {
        $path_parts = explode('/', $path);
        if (count($path_parts) == 1) {
            if ($path != 'empty_file') {
                $files[] = $path;
            } elseif(empty($files)) {
                $files = array();
            }
        } else {
            $folder = $path_parts[0];
            unset($path_parts[0]);
            $this->buildTree(&$files[$folder], implode('/', $path_parts));
        }
        return 1;
    }
}