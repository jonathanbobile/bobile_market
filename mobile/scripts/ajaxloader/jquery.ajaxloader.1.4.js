/**
 * Ajaxloader plugin 1.4
 *
 * Copyright (c) 2010 Blokhin Yuriy (ds@inbox.ru)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
 /**
 * Ajaxloader plugin using for disable content while loading data and display preload image. 
 */
/**
 * @version 1.4
 *
 * @example  
 *              <div id='some-content'> Some content </div>
                <button>Some button</button>
                $(function(){
                    $('button').click(function(){
                        $('#some-content').ajaxloader();
                        $('#some-content').load('some_page.html'); 
                    });
                });
 * @param 
 *      mode:
 *              hide - hide preload image
 *              show/null - show preload image
 *      options:
 *      'tata-ajax-loader-img' - class of preload image ( .tata-ajax-loader-img { background-image: url('ajax-loader.gif');} )
 *         or
 *      options = {
 *       imageclass : 'tata-ajax-loader-img', - class of preload image
 *       fade: true, - fade enable
 *       fadespeed: 'fast' -fade speed
 *       }
 *              
 * @return nothing
 * @cat Plugins/Ajax
 * @author Blokhin Yury/ds@inbox.ru
 */
jQuery.fn.ajaxloader = function(mode, options) {
   if (typeof(options)!='array'){
        if (typeof(options)=='string'){ image = options;}else {image = 'tata-ajax-loader-img';};
        options = { 
            imageclass : image,
            fade:   false,
            fadespeed:   'fast'
        };
   }
   if ( !($(this).children('.tata-ajax-loader').length)  ){

       $(this).prepend("<div class='tata-ajax-loader spinner-container' style='position:absolute; display: none; opacity: 0.8;background-color: #fff; z-index: 99999999999; filter:alpha(opacity=80);'><div class='content'><div class='loader-circle'></div><div class='loader-line-mask'><div class='loader-line'></div></div></div></div>");
         //$(this).children('.tata-ajax-loader').children('div').addClass(options.imageclass);
   }
   //<div style='background-position: center;background-repeat: no-repeat;height:100%;width:100%;background-color: transparent;'></div>

   $(this).children('.tata-ajax-loader').width($(this).width());
   $(this).children('.tata-ajax-loader').height($(this).height());
   if ( mode == null || mode == 'show'){
         // if ($(this).children('.tata-ajax-loader').children('div').attr('class')!=options.imageclass){
         //     $(this).children('.tata-ajax-loader').children('div').removeClass($(this).children('.tata-ajax-loader').children('div').attr('class'));
         //     $(this).children('.tata-ajax-loader').children('div').addClass(image);
         // }
         if (options.fade)
             $(this).children('.tata-ajax-loader').fadeIn(options.fadespeed);
         else
          
            $(this).children('.tata-ajax-loader').show();
   }else{
        if ( mode == 'hide'){
          
            if (options.fade){
                $(this).children('.tata-ajax-loader').fadeOut(options.fadespeed);
            }else{
                $(this).children('.tata-ajax-loader').hide();
            }
            
        }
   }
};
