﻿(function ($) {

    
    $.popover = function (options) {
        var settings = $.extend({
            width: 700,
            height: 500,
            onOpen: '',
            onClose: '',
            type: 'iframe',
            href: 'http://www.ynet.co.il',
            ajaxData: {},
            html: '',
            closebutton: '',
            blockui: false,
            customClass: '',
            lockScroll: true,
            originElement: $(''),
            targetElement: $(''),
            topOffset: 0,
            leftOffset: 0,
            noX: false
        }, options);

        var background = '<div class="popover-background"></div><div class="popover-container ' + settings.customClass +'" style="opacity:0;"><div class="popover-close"></div><div class="popover-content"></div></div>';
        $('body').append(background);



        switch (settings.type) {
            case 'iframe':
                $('.popover-content').html('');
                $('.popover-content').css('padding','0px');
                openPopover('<iframe class="inner-iframe" src="' + settings.href + '"></iframe>',true);
                break;
            case 'ajax':
                $('.popover-container .popover-content').ajaxloader('show');
                $.ajax({
                    type: 'POST',
                    data: settings.ajaxData,
                    url: settings.href,
                    success: function (data) {
                        $('.popover-content').html('');
                        openPopover(data,false);

                    }
                });
                break;
            default:
                $('.popover-container .popover-content').html('');
                openPopover(settings.html,false);
                break;
        }




       


        if (!settings.blockui) {
            $('.popover-background').click(function () {
                if ($.isFunction(settings.onClose)) {
                    settings.onClose();
                }
                closePopover();
            });
        }

        $(settings.closebutton).click(function () {
            if ($.isFunction(settings.onClose)) {
                settings.onClose();
            }
            closePopover();
        });

        $('.popover-container').data('settings', settings);

        function closePopover() {
            $('.popover-background').removeClass('cover');
            $('.popover-background').remove();
            $('.popover-container').remove();
            if (settings.lockScroll) {
                $("body").css("overflow", "initial");
            }
        };

        function openPopover(html, showSpinner) {
          
            var offTop = ($(window).height() - settings.height) / 2;
            var offLeft = ($(window).width() - settings.width) / 2;
            if (window.location != window.parent.location) {
                offTop = ($('body').height() - settings.height) / 2;
                offLeft = ($('body').width() - settings.width) / 2;
            }

            if (settings.targetElement.length != 0) {
                offTop = settings.targetElement.offset().top - (settings.height / 2) - (settings.targetElement.outerHeight() / 2);
                offLeft = settings.targetElement.offset().left - (settings.width / 2) - (settings.targetElement.outerWidth() / 2);
            }

            var originLeft = offLeft;
            var originTop = offTop;

            if (settings.originElement.length != 0) {
                originLeft = settings.originElement.offset().left;
                originTop = settings.originElement.offset().top;

            }

            $('.popover-container').width(10).height(10).css('top', originTop).css('left', originLeft).css('opacity',0);
            $('popover-container .popover-close').css('display', 'none');

            $('.popover-background').addClass('cover');

            var midHorizontal = offLeft + settings.width / 2;
            var midVertical = offTop + settings.height / 2;
            $('.popover-container').css({ 'left': midHorizontal, 'top': midVertical,'width':'10px', 'height':'10px' });

            $('.popover-container').animate({
                opacity: 1,
                left: offLeft - settings.leftOffset,
                top: offTop - settings.topOffset,
                width: settings.width,
                height: settings.height
            }, 500, function () {

                $('.popover-container .popover-content').html(html);
                if (showSpinner){
                    $('.popover-container .popover-content').ajaxloader('show');
                }
                
                if (settings.noX) {
                    $('.popover-container .popover-close').hide();
                }
                else {
                    $('.popover-container .popover-close').show();
                }
                $(settings.closebutton).click(function () {
                    if ($.isFunction(settings.onClose)) {
                        settings.onClose();
                    }
                    closePopover();
                });
                if ($.isFunction(settings.onOpen)) {
                    settings.onOpen();
                }
            });

            $('.popover-container ').on('click','.popover-close, .popover-cancel',function () {
                if ($.isFunction(settings.onClose)) {
                    settings.onClose();
                }
                closePopover();
            });

            

            if (settings.blockui) {
                $('.popover-background').addClass('opaque');
            }
            
            if (settings.lockScroll) {
                $("body").css("overflow", "hidden");
            }
        }
    }

    $.popover.close = function () {
        var func = $('.popover-container').data('settings').onClose;
        if ($('.popover-container').data('settings').lockScroll) {
            $("body").css("overflow", "initial");
        }
        if ($.isFunction(func)) {
            func();
        }
        $('.popover-background').removeClass('cover');
        $('.popover-background').remove();
        $('.popover-container').remove();
        
        
    }


    

            
            
            





        
   
}(jQuery));