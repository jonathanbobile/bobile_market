/// <reference path="global.js" />
$(document).ready(function () {

    $(".mainRight").css("width","100%");
    $(".mainRight").css("margin-left","0px");
    $(".logo-holder").css("left","0px");
    $(".mainleftmenuNew, .app-box, .notifi-box").css("display","none");
    $(".logo-holder").css("margin-left","0px");

});

function init_account_pcas() {
    $('.markets-holder').ajaxloader('show');
    $('.search-markets').attr('disabled', true);
    $.ajax({
        type: 'GET',
        url: URL + 'markets/marketsList',
        success: function (data) {
            $('.markets-holder').ajaxloader('hide');
            $('#markets').html(data);
            if ($('.box.market').length == 0) {
                $('#no_markets').removeClass('hidden-tag');
            }
            $('#totalMarkets').html($('.box.market').length);
            $('.search-markets').attr('disabled', false);
        }
    });

    $('.add-market').click(function () {



        $.bobup({
            ajaxURL:URL + 'markets/newMarket',
            actions:{
                create:{
                    label:Language.create_market,
                    action:function(){
                        var flag = false;
                        $('.new-market input.required').each(function () {
                            $(this).removeClass('error');
                            if ($.trim($(this).val()) == '') {
                                flag = true;
                                $(this).addClass('error');
                            }
                        });

                        if (flag) {
                            ptAlert({
                                message: Language.missing_fields
                            });
                            return;
                        }

                        var withdemo = $('[name=withdemo]').prop('checked') ? 1 : 0;
                        $('.markets-holder').ajaxloader('show');
                        $('.bobup-body ').ajaxloader('show');
                        $.ajax({
                            type: 'POST',
                            url: URL + 'markets/addMarket',
                            data: { name: $('[name=name]').val(), store_name: $('[name=store_name]').val(), withdemo: withdemo },
                            success: function (data) {
                                $('.bobup-body ').ajaxloader('hide');
                                $('.markets-holder').ajaxloader('hide');
                                if (typeof data.responseCode != 'undefined') {
                                    ptAlert();
                                }
                                else {
                                    $('#markets').append(data);
                                    $('#no_markets').addClass('hidden-tag');
                                    $.bobup.close();
                                }
                            }
                        });
                    }
                }
            }

        });

        // $.fancybox({
        //     width: fncyWidth,
        //     height: fncyHeight,
        //     padding: 0,
        //     autoDimensions: false,
        //     href: URL + 'markets/newMarket',
        //     type: 'ajax',
        //     scrolling: false,
        //     hideOnOverlayClick: false,
        //     onComplete: function () {
        //         initNewMarket();
        //     }
        // });
    });

    $('.markets-holder').on('click', '.delete-market', function () {
        var elem = $(this).closest('.market.box');
        var marketid = $(this).closest('.market.box').data('marketid');
        ptConfirm({
            header:Language.delete_market_confirm_header,
            message: Language.delete_market_confirm,
            type:'warning'

        }, function () {
            $.ajax({
                type: 'GET',
                url: URL + 'markets/deleteMarket/' + marketid,
                success: function (data) {
                    if (data.responseCode == 1) {
                        elem.remove();
                        if ($('.box.market:not(.hidden-tag)').length == 0) {
                            $('#no_markets').removeClass('hidden-tag');
                        }
                    }
                    else {
                        ptAlert();
                    }
                }
            });
        });
        
    });

    $('.search-markets').keyup(function () {
        var val = $(this).val().toLowerCase();

        $('.box.market').each(function () {
            var name = $(this).data('marketname').toLowerCase();

            if (name.includes(val) || val == '') {
                $(this).removeClass('hidden-tag');
            }
            else {
                $(this).addClass('hidden-tag');
            }
        });

        if ($('.box.market:not(.hidden-tag)').length == 0) {
            $('#no_markets').removeClass('hidden-tag');
        }
        else {
            $('#no_markets').addClass('hidden-tag');
        }

        $('#totalMarkets').html($('.box.market:not(.hidden-tag)').length);
    });

    $('.markets-holder').on('click', '.manage-market', function () {
        var marketid = $(this).closest('.market.box').data('marketid');

        $.ajax({
            type: 'GET',
            url: URL + 'markets/selectMarket/' + marketid,
            success: function (data) {
                if (data.responseCode == 1) {
                    window.location.href = '/admin/markets/marketApps';
                }
                else {
                    ptAlert();
                }
            }
        });
    });
}

function init_account_myapps() {
    $("#createApp").off("click");
    $("#createApp").on("click", function () {
        if ($(this).hasClass('max-apps')) {
            ptConfirm({
                message: Language.max_reseller_apps,
                btnOK: Language.upgradeNeededConfirm
            }, function () {
                document.location.href = "/admin/addons/reseller_programs";
            });
        }
        else {
            document.location.href = "/admin/credential/register/addApp";
        }

    });

    $(document).on('click', '.delete-app', function () {
        var elem = $(this);
        ptConfirm({}, function () {
            var appid = elem.data('app');
            $('#apps').ajaxloader('show');
            $.ajax({
                type: 'POST',
                url: URL + 'account/removeApp',
                data: { appid: appid },
                success: function (data) {
                    if (data.res == 1) {
                        elem.closest('.app').remove();
                        var count = $('#apps .app').length;
                        $('.apps-counter #totalApps').text(count);
                        if (count == 0) {
                            $('#apps').ajaxloader('show');

                            $.ajax({
                                url: URL + 'account/apps',
                                type: 'POST',
                                success: function (data) {
                                    $('.create-app').removeClass('hidden-tag');
                                    $('#apps').html(data);
                                    $('#apps').ajaxloader('hide');
                                }
                            });
                        }
                        else {
                            $('#apps').ajaxloader('hide');
                        }
                    }
                    else {
                        ptAlert();
                    }
                }
            });
        });

    });

    $('#apps').ajaxloader('show');
    var view = '';
    if (isMobile) {
        view = '/modal';
    }
    $('.search-apps').attr('disabled', true);
    $.ajax({
        url: URL + 'account/apps' + view,
        type: 'POST',
        success: function (data) {
            $('#apps').html(data);
            
            $('#apps').ajaxloader('hide');
            if ($(".app").length == 0 || data == '') {
                $('.no-apps').removeClass('hidden-tag');
            }
            else {
                $('#apps').ptInfiniteScroll({
                    url: 'account/apps',
                    itemsPerCall: 10,
                    ajaxOptionalParmas: { qs: $('.search-apps').val() },
                    onScroll: function () {
                        $('.load-more-apps').ajaxloader('hide');
                    },
                    beforeScroll: function () {
                        $('.load-more-apps').ajaxloader('show');
                    }

                });
            }
            $('.search-apps').attr('disabled', false);
        }
    });

    

    $('.search-apps').keyup(function (e) {
        if (e.keyCode == 13) {
            $('#apps').ajaxloader('show');
            $.ajax({
                url: URL + 'account/apps',
                type: 'POST',
                data: { qs: $('.search-apps').val() },
                success: function (data) {
                    $('#apps').html(data);

                    $('#apps').ajaxloader('hide');
                    if ($(".app").length == 0 || data == '') {
                        $('.no-apps').removeClass('hidden-tag');
                    }
                    else {
                        $('#apps').ptInfiniteScroll.settings['ajaxOptionalParmas'] = { qs: $('.search-apps').val() };
                        $('#apps').ptInfiniteScroll.reset();
                    }
                
                }
            });
        }
       
    });

    

    $(document).on('click', '.invite-client', function () {
        var appId = $(this).data('app');
        $.fancybox({
            width: 740,
            height: 400,
            padding: 0,
            autoDimensions: false,
            href: URL + 'account/inviteClient',
            type: 'ajax',
            ajax: {
                type: "POST",
                data: {app: appId}
            },
            scrolling: false,
            hideOnOverlayClick: false,
            onComplete: function () {
                
               

                
                $('.cancel-invite').click(function () {
                    $.fancybox.close();
                });
                $('.send-invite').click(function () {
                    if ($('.invite-client-form input.client-mail').val() == "") {
                        ptAlert({
                            message: Language.please_enter_email
                        });
                        return;
                    }
                    var name = $('.invite-client-form input.client-mail').val();
                    if (!validateEmail(name)) {
                        ptAlert({
                            message: Language.please_enter_valid_email
                        });
                        return;
                    }
                    
                    
                    var appid = $(this).data('app');
                   
                    $.ajax({
                        type: 'POST',
                        url: URL + 'account/addResellerClient',
                        data: { app: appid, client_name: name },
                        success: function (data) {
                            if (data != "1" && data != -1 && data != -2) {
                                $('.row[data-app="' + appid + '"]').replaceWith(data);
                                $.fancybox.close();
                            }
                            else {
                                ptAlert({
                                    message:Language.biz_already_managed
                                });
                            }
                        }
                    });

                });
            }
        });
    });
}

function init_client_list() {
    $('input.switcher').each(function () {
        $(this).ptswitch();
    });

    $('.client-list-header input[name=select-all]').change(function () {
        var state = $(this).prop('checked');
        $('.client-list-holder .select-client').each(function () {
            $(this).prop('checked', state);
        });
    });

    $('.client-list-holder .switch-element').click(function () {
       
        var stateNow = $(this).find('input').prop('checked') ? "1" : "0";
        var clientId = $(this).closest('.client-list-elem').data('clientid');
        var appid = $(this).closest('.client-list').data('app');

        $.ajax({
            type: 'POST',
            url: URL + 'account/switchClientApp',
            data: { client: clientId, app: appid, state: stateNow },
            success: function (data) {
            }
        });
    });

   

    $('.select-all-clients + span, .delete-selected').click(function () {
        if ($('.client-list-holder .client-list-elem .select-client:checked').length == 0) {
            return;
        }
        var appid = $(this).closest('.client-list').data('app');
        ptConfirm({
            message: Language.confirm_delete_user
        }, function () {
            var toDel = "";
            $('.client-list-holder .client-list-elem .select-client:checked').each(function () {
                var id = $(this).closest('.client-list-elem').data('clientid');
                if (toDel != "") {
                    toDel += ",";
                }
                toDel += id.toString();
            });
            
            $.ajax({
                url: URL + 'account/deleteClients',
                type: 'POST',
                data: { delList: toDel },
                success: function (data) {
                    if (data == "1") {
                        
                        $('.client-list-holder .client-list-elem .select-client:checked').each(function () {
                            $(this).closest('.client-list-elem').remove();
                        });
                        var count = $('.client-list-holder .client-list-elem').length;
                        var entry = $('div#apps .row[data-app="' + appid + '"]').find('.client-count').text(count);
                    }
                    else {
                        ptAlert();
                    }
                }
            });
        });
    });
}

function init_account_settings() {

    $('#saveAccountSettings').click(function () {
        $("#password").removeClass('error');
        if ($("#password").val() != '' && ($.trim($("#password").val()) == '')) {
            $("#password").addClass('error');
            ptAlert({ message: Language.missing_fields });
            return false;
        }

        if (!$(this).hasClass('reseller-account')) {
            if ($.trim($("#contactName").val()) == "") {
                ptAlert({ message: Language.fill_contact_name });
                return false;
            }

            if ($("#password").val() != $("#password1").val()) {
                ptAlert({ message: Language.wrong_confirm_password });
                return false;
            }
            

            var data = {
                cname: $("#contactName").val(),
                password: $("#password").val(),
                notiLang: $('#notif-language').length > 0 ? $('#notif-language').val() : '',
                sysLang: $('#system-language').val()
            };

            $(".tab-content-canvas").ajaxloader('show');
            $.ajax({
                data: data,
                type: 'POST',
                url: "/admin/account/saveSettings",
                success: function (response) {

                    response = JSON.parse(response);
                    if (response.responseCode === -1) {
                        ptAlert({ message: response.responseMessage });
                    }
                    else {
                        ptAlert({
                            type: 'confirmed',
                            header: Language.saved_successfully,
                            message: Language.settings_saved,
                            onOk: function () {
                                window.location.reload();
                            }
                        });
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    }
                    $(".tab-content-canvas").ajaxloader('hide');
                }
            });
        }
        else {
            startSubmit();
        }
    });


    $('select').each(function () {
        if ($(this).find('option').length < 4) {
            $(this).select2({
                minimumResultsForSearch: -1
            });
        }
        else {
            $(this).select2();
        }
    });

    $('#password').keyup(function () {
        if ($.trim($(this).val()) != "") {
            $('#password1').prop('disabled', false);
        }
        else {
            $('#password1').prop('disabled', true);
        }

        if ($('#password1').val().indexOf($(this).val()) < 0) {
            $('#password1').addClass('error');
            $('#password1').addClass('no-match');
        }
        else {
            if ($('#password1').val().length == $(this).val().length) {
                $('#password1').removeClass('no-match');
            }
        }
    });

    $('#password1').keyup(function () {
        $(this).removeClass('error');
        if ($('#password').val().indexOf($(this).val()) < 0) {
            $(this).addClass('error');
            $(this).addClass('no-match');
        }
        else {
            if ($('#password').val().length == $(this).val().length) {
                $(this).removeClass('no-match');
            }
        }
    });

    $('#business-type').change(function () {
        if ($(this).val() == 'Individual') {
            $('.company-info').addClass('hidden-tag');
        }
        else {
            $('.company-info').removeClass('hidden-tag');
        }
    });

    $('#country').change(function () {
        if ($(this).val() == 1) {
            $('select[id=state]').closest('.state-wrapper').removeClass('hidden-tag');
        }
        else {
            $('select[id=state]').closest('.state-wrapper').addClass('hidden-tag');
        }

        var code = $(this).find('option:selected').data('code');
        $('input#phone').intlTelInput("setCountry", code);
    });

    $('input#phone').intlTelInput({
        utilsScript: URL + "scripts/intl-tel-input-master/build/js/utils.js",
        autoPlaceholder: "aggresive",
        initialCountry: $('#country').find('option:selected').data('code')
    });
}

function init_account_emails() {
    
    $('.email-edit').click(function () {
        emailId = $(this).closest("tr").data("id");
        changeTabContent('account', 'email_data', { emailid: emailId, partial: 1 }, init_account_email, '#tab-content-canvas', '', 'fromRight');
    });

    $('.push-edit').click(function () {
        pushId = $(this).closest("tr").data("id");
        changeTabContent('account', 'push_data', { pushId: pushId, partial: 1 }, init_account_email);
    });
}

function init_account_email() {

    $('.cancel-edit').click(function () {
        changeTabContent('account', 'emails', { partial: 1 }, init_account_emails, '#tab-content-canvas', '', 'fromLeft');
    });

    $('.reset-email').click(function () {

        emailId = $(this).closest(".email-data").data("emailid");
        var data = {
            eid: emailId,
            oid: $(this).closest(".email-data").data("emailoid")
        };

        ptConfirm({}, function () {
            $('.tab-content-canvas').ajaxloader('show');
            $.ajax({
                type: 'POST',
                url: URL + 'account/resetEmail',
                data: data,
                success: function (data) {
                    $('.tab-content-canvas').ajaxloader('hide');
                    if (data.responseCode == 1) {

                        changeTabContent('account', 'email_data', { emailid: emailId, partial: 1 }, init_account_email);
                    }
                    else {
                        ptAlert();
                    }
                }
            });
        });
    });

    $('.reset-push').click(function () {

        pushid = $(this).closest(".email-data").data("pushid");
        var data = {
            pid: pushid,
            oid: $(this).closest(".email-data").data("pushoid")
        };

        ptConfirm({}, function () {

            $('.tab-content-canvas').ajaxloader('show');
            $.ajax({
                type: 'POST',
                url: URL + 'account/resetPush',
                data: data,
                success: function (data) {
                    $('.tab-content-canvas').ajaxloader('hide');
                    if (data.responseCode == 1) {

                        changeTabContent('account', 'push_data', { pushId: pushid, partial: 1 }, init_account_email);
                    }
                    else {
                        ptAlert();
                    }
                }
            });

        });
    });

    $('.colorSelector').ColorPicker({
        onSubmit: function (hsb, hex, rgb, el) {
            selectedElement.css('background-color', '#' + hex);
            selectedElement.data("color", '#' + hex);
            $("." + selectedElement.data("effects")).css(selectedElement.data("effects-element"), '#' + hex);
            $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
            selectedElement = $(this);
            $(this).ColorPickerSetColor(hexc($(this).css('background-color')).replace('#', ''));
        }
    });

    $(".edit-element-button").click(function () {

        var parentElement = $(this).prev();
        var txtElement;

        if (parentElement.hasClass("dataDiv")) {
            txtElement = parentElement;
        } else {
            txtElement = parentElement.find(".dataDiv");
        }

        $.bobup({
            type: 'ajax',
            ajaxURL: URL + 'account/edit_email_text',
            customClass: 'edit-email-popup',
            actions: {
                save: {
                    label: Language.save,
                    action: function () {

                        if (txtElement.data("pt-req") == "1" && $("#emailData").val().trim() == "") {
                            $("#emailData").addClass("required_input");
                            ptAlert({ message: Language.fill_required });
                        } else {
                            txtElement.html($("#emailData").val().replace(/\n/g, "<br />"));
                            $.bobup.close();
                        }
                    }
                }
            },
            onOpen: function () {
                $("#emailData").val(txtElement.html().trim().replace(/<br\s*[\/]?>/gi, "\n"));
            }
        });

    });

    $('.save-email').click(function () {

        canSave = true;
        $(".textField").each(function () {

            var req = $(this).data("pt-req");
            var value = $(this).val();
            $(this).removeClass("required_input");

            if ((req == "1" && $.trim(value) == '')) {
                $(this).addClass("required_input");
                canSave = false;
            }

        });

        if (!canSave) {
            ptAlert({ message: Language.fill_required });
            return false;
        }

        var data = {
            eid: $(this).closest(".email-data").data("emailid"),
            oid: $(this).closest(".email-data").data("emailoid"),
            subject: $("#subject").val(),
            backColor: $("#backColor").data("color"),
            textColor: $("#textColor").data("color"),
            buttonColor: $("#buttonColor").data("color"),
            textHeader: $("#textHeader").html(),
            textText1: $("#textText1").html(),
            textText2: $("#textText2").html(),
            textText3: $("#textText3").html(),
            textFooter: $("#textFooter").html(),
            textButton: $("#textButton").html(),
            from: $("#from").val()
        };

        $('.tab-content-canvas').ajaxloader('show');
        $.ajax({
            type: 'POST',
            url: URL + 'account/saveEmail',
            data: data,
            success: function (data) {
                $('.tab-content-canvas').ajaxloader('hide');
                if (data.responseCode == 1) {

                    ptAlert({
                        type: 'confirmed',
                        header: Language.saved,
                        message: Language.saved_successfully
                    });
                }
                else {
                    ptAlert();
                }
            }
        });

    });

    $('.save-push').click(function () {

        canSave = true;
        $(".textField").each(function () {

            var req = $(this).data("pt-req");
            var value = $(this).val();
            $(this).removeClass("required_input");

            if ((req == "1" && $.trim(value) == '')) {
                $(this).addClass("required_input");
                canSave = false;
            }

        });

        if (!canSave) {
            ptAlert({ message: Language.fill_required });
            return false;
        }

        var data = {
            pid: $(this).closest(".email-data").data("pushid"),
            oid: $(this).closest(".email-data").data("pushoid"),
            message: $("#pushMessage").val()
        };

        $('.tab-content-canvas').ajaxloader('show');
        $.ajax({
            type: 'POST',
            url: URL + 'account/savePush',
            data: data,
            success: function (data) {
                $('.tab-content-canvas').ajaxloader('hide');
                if (data.responseCode == 1) {

                    ptAlert({
                        type: 'confirmed',
                        header: Language.saved,
                        message: Language.saved_successfully
                    });
                }
                else {
                    ptAlert();
                }
            }
        });

    });

    $('.email-data').on('keyup', '#pushMessage', function () {
        $('.phone .text').html($(this).val());
    });
}

function init_account_wl_settings() {
    $('.wlColorSelector').ColorPicker({
        onSubmit: function (hsb, hex, rgb, el) {
            selectedElement.css('background-color', '#' + hex);
            selectedElement.data("color", '#' + hex);
            $(el).ColorPickerHide();
            if (selectedElement.hasClass('app-color')) {
                var color = hexc($('.app-color').css('background-color'));

                var lum = getLumin(color) / 2.55;

                if (lum  >= 70) {
                    ptAlert({
                        type: 'warning',
                        header:Language.warning,
                        message:Language.app_manager_color_dark
                    });
                }
            }
        },
        onBeforeShow: function () {
            selectedElement = $(this);
            $(this).ColorPickerSetColor(hexc($(this).css('background-color')).replace('#', ''));
        }
    });

    $('.developerAccount').click(function () {
        openGoogleAccountCreator();
    });

    $('input.switcher').each(function () {
        $(this).ptswitch();
    });

    $('.reseller-image-field').change(function () {
        var type = $(this).data('type');
        var files = $(this)[0].files;
        var fdata = new FormData();
        var accepted = $(this).data('accepted');
        var acceptedFiles = true;
        $.each(files, function (i, file) {
            if (typeof accepted != 'undefined' && accepted != '') {
                var acceptedTypes = accepted.split(',');
                var fileExplode = file.name.split('.')
                var fileType = fileExplode[fileExplode.length - 1];
                flag = false;
                for (var j = 0; j < acceptedTypes.length; j++) {
                    if (fileType == acceptedTypes[j]) {
                        flag = true;
                    }
                }

                if (!flag) {
                    acceptedFiles = false;
                }
            }
            fdata.append("pic_" + i, file);
        });
        if (!acceptedFiles) {
            var text = Language.wrong_file_type.replace('#accepted_types#', accepted);
            ptAlert({
                message: text
            });
            return;
        }
        $('.' + type).ajaxloader('show');
        $.ajax({
            type: 'POST',
            url: URL + 'account/upload_reseller_image',
            data: fdata,
            processData: false,
            contentType: false,
            success: function (data) {
                $('.' + type).ajaxloader('hide');
                if (data.responseCode == 1) {
                    
                    $('.' + type + ' > img').attr('src', data.responseMessage);
                }
                else {
                    ptAlert({
                        message: Language.image_upload_fail
                    });
                }
            }
        });
    });

    



    $('.file-img-upload').change(function (e) {
        var file = e.target.files[0];
        var img = $(this).data('img');
        var isAppLogo = $(this).hasClass('app-logo-uploader');
        if (!file.type.match('image/jpeg') && !isAppLogo) {
            ptAlert({
                message: Language.file_not_image_jpeg
            });
            var size = '84';
            if ($(this).attr('id') == 'splash_upload') {
                size = '200';
            }
            $('#' + img).attr('src', '/admin/img/camera_' + size + '.png');
            return;
        }

        var type = $(this).data('type');
        var formData = new FormData();
        formData.append('file', $(this)[0].files[0], $(this)[0].files[0].name);
        formData.append('type', type);
        $(this).val('');
        $('.inner-content-wrapper').ajaxloader('show');
        $.ajax({
            type: 'POST',
            url: URL + 'account/update_reseller_app_image',
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {
                $('#' + img).attr('src', data.responseMessage);
                $('.inner-content-wrapper').ajaxloader('hide');
                var options = {};
                if (typeof $('#' + img).data('shape') != 'undefined') {
                    options = {
                        cropPresetDefault: 'Square',
                        cropPresets: ['Square', '1:1'],
                        cropPresetsStrict: true,
                        forceCropPreset: [$('#' + img).data('shape'), $('#' + img).data('fwidth') + ':' + $('#' + img).data('fheight')],
                        forceCropMessage: Language.please_crop_your_image + ': ' + $('#' + img).data('fwidth') + ' x ' + $('#' + img).data('fheight'),
                        hiresUrl: $('#' + img).attr('src'),
                        url: $('#' + img).attr('src')

                    };
                }

                if (!isAppLogo) {
                    $('#' + img).editImage(options, function (id, url) {
                        var data = {
                            remote: 1,
                            src: $('#' + img).attr('src'),
                            type: type
                        };
                        $.ajax({
                            type: 'POST',
                            url: URL + 'account/update_reseller_app_image',
                            data: data,
                            success: function (data) {
                                if (data.responseCode == 1) {
                                    $('#' + img).attr('src', data.responseMessage);
                                    ptAlert({
                                        type: '',
                                        message: Language.image_saved,
                                        header: Language.image_saved_header
                                    });

                                }
                            }
                        });
                    });
                }
            }
        });




    });

    $('.save-settings').click(function () {

        var intake = {};
        var error = {
            empty: false,
            email: false,
            url: false,
            image: false,
            appcolor: false
        };
        $('.wl-settings input:not([type=file]):not([type=hidden]):not([disabled]), .wl-settings textarea').each(function () {
            if ($(this).attr('type') == 'checkbox') {
                intake[$(this).attr('id')] = $(this).prop('checked') ? 1 : 0;
            }
            else {
                if (typeof $(this).data('req') != 'undefined') {
                    
                    $(this).removeClass('error');
                    if ($.trim($(this).val()) == "") {
                        $(this).addClass('error');

                        error.empty = true;

                    }
                    

                   

                }

                if (typeof $(this).data('check') != 'undefined') {
                    if ($(this).data('check') == 'email') {
                        if (!validateEmail($(this).val())) {
                            $(this).addClass('error');

                            error.email = true;
                        }

                    }
                    if ($(this).data('check') == 'url') {
                        if (!validateUrl($(this).val())) {
                            $(this).addClass('error');

                            error.url = true;
                        }
                    }
                }

                intake[$(this).attr('id')] = $(this).val();
            }

            
        });

        $('img.imgField').each(function () {
            $(this).closest('.element-img-wrapper').removeClass('error');
            if ($(this).attr('src').indexOf('img/camera_') > 0 && $(this).closest('.module-part#app').length == 0 && $(this).data('req') == 1) {
                $(this).closest('.element-img-wrapper').addClass('error');
                error.image = true;
            }
            else {
                intake[$(this).data('field')] = $(this).attr('src');
            }
        });
       

        $('.wlColorSelector').each(function () {
            intake[$(this).data('field')] = $(this).data('color');
        });

        if ($('.app-color').length > 0) {
            var color = hexc($('.app-color').css('background-color'));

            var lum = getLumin(color) / 2.55;

            if (lum >= 70) {
                error.appcolor = true;
            }

        }

        var errMessage = "";
        if (error.empty) {
            errMessage = Language.fill_required;
        }
        else {
            if (error.email) {
                if (errMessage != "") errMessage += "</br>";
                errMessage += '* ' + Language.wrong_email_format;
            }
            if (error.url) {
                if (errMessage != "") errMessage += "</br>";
                errMessage += '* ' + Language.wrong_url;
            }
            if (error.image) {
                if (errMessage != "") errMessage += "</br>";
                errMessage += '* ' + Language.add_image;
            }
            if (error.appcolor) {
                if (errMessage != "") errMessage += "</br>";
                errMessage += '* ' + Language.app_manager_color_dark;
            }
        }

        if (errMessage != "") {
            if ($('.error').first().closest('.module-part').hasClass('hidden-tag')) {
                var current = $(".inner-tab.selected").data('inner-tab');
                var new_tab = $('.error').first().closest('.module-part').attr('id');
                switchInnerTab(current, new_tab);
            }
            ptAlert({
                message: errMessage
            });
            return;
        }
        $('.transparent-content-wrapper').ajaxloader('show');
        $.ajax({
            type: 'POST',
            url: URL + 'account/update_wl_settings',
            data: intake,
            success: function (data) {
                
                if (data.responseCode == 1) {
                    ptAlert({
                        header: Language.saved,
                        type: 'confirmed',
                        message: Language.wl_settings_saved
                    });
                }
                else {
                    ptAlert({
                        message: Language.responseMessage
                    });
                }
            },
            complete: function () {
                $('.transparent-content-wrapper').ajaxloader('hide');
            }
        });
    });
}

function init_account_billing() {
    
    $('.invoiceDetails').click(function () {

        $.bobup({
            type: 'ajax',
            ajaxURL: URL + 'addons/invoice_details/' + $(this).data('tr-id')
        });
    });

    //update credit card reseller
    $('.changeMethod').click(function () {

        $('.mainRight').ajaxloader('show');
        $.ajax({
            url: URL + 'checkout/refreshZoozLoginToken',
            type: 'POST',
            success: function (response) {
                $('.mainRight').ajaxloader('hide');
                response = JSON.parse(response);
                if (response.responseCode === -1) {
                    ptAlert({ message: response.responseMessage });
                }
                else if (response.responseCode === -2) {
                    window.location.href = '/admin/credential/login';
                } else {

                    var invoiceItems = { 'item': {} };

                    pInvoiceItems = JSON.stringify(invoiceItems);

                    var invoceData = {
                        items: pInvoiceItems,
                        markAsPayd: false,
                        updateOnly: true
                    };

                    $.fancybox({
                        width: 850,
                        height: 600,
                        padding: 0,
                        scrolling: 'no',
                        autoDimensions: false,
                        href: URL + 'checkout/loadPaymentForm',
                        type: 'ajax',
                        ajax: {
                            type: "POST",
                            data: invoceData
                        },
                        hideOnOverlayClick: false
                    });
                }
            }
        });
    });

}

function startSubmit() {
    var flag = false;
    var mismatch = false;
    var data = {};
    $('.tab-content-canvas').find('input,select,textarea').each(function () {
       
        $(this).removeClass('error');
        if ($(this).closest('.hidden-tag').length > 0) {
            return;
        }
        if ($(this).hasClass('no-match')) {
            $(this).addClass('error');
            flag = true;
            mismatch = true;
            return;
        }
        if ($(this).data('req') == 1) {
            if ($.trim($(this).val()) == '') {
                $(this).addClass('error');
                flag = true;
                return;
            }
            if ($(this).data('check') == 'email') {
                var result = validateEmail($(this).val());
                if (result != '') {
                    $(this).addClass('error');
                    flag = true;
                    return;
                }
            }
            if ($(this).data('check') == 'phone') {
                if (!$(this).intlTelInput("isValidNumber")) {
                    $(this).addClass('error');
                    flag = true;
                    return;
                }

            }
        }


        if ($(this).attr('id') != 'phone') {
            data[$(this).attr('id')] = $(this).val();
        }
        else {
            data['phone'] = $('input#phone').intlTelInput("getNumber");
        }

    });

    if (flag) {
        var message = mismatch ? Language.wrong_confirm_password : Language.missing_fields;
        ptAlert({ message: message });
        return;
    }

    $(".tab-content-canvas").ajaxloader('show');
    $.ajax({
        type: 'POST',
        url: URL + 'account/update_reseller',
        data: data,
        success: function (data) {
            response = JSON.parse(data);
            $(".tab-content-canvas").ajaxloader('hide');
            if (response.responseCode === -1) {
                ptAlert({ message: response.responseMessage });
            }
            else {
                ptAlert({
                    type: 'confirmed',
                    header: Language.saved_successfully,
                    message: Language.settings_saved,
                    onOk: function () {
                        window.location.reload();
                    }
                });
                setTimeout(function () {
                    window.location.reload();
                }, 1500);
            }
        }
    });
    
}

function openGoogleAccountCreator() {

    $.fancybox({
        width: 1050,
        height: 768,
        padding: 0,
        autoDimensions: false,
        href: URL + 'account/googleAccountCreator',
        type: 'ajax',
        scrolling: false,
        hideOnOverlayClick: false,
        onComplete: function () {

            $('.googleFrameWrapper').ajaxloader('show');

            var messageEventHandler = function (event) {
                if (event.data == 'appLaunch') {

                    setTimeout(function () {
                        $('#googleFrame').removeClass('hidden-tag');
                        $('.googleFrameWrapper').ajaxloader('hide');
                    }, 4000);
                }
            };

            window.addEventListener('message', messageEventHandler, false);
            $(".googleFrame").closest("#fancybox-content").css("overflow", "auto");
        },
        onClosed: function () {

            var data = {};

            $.ajax({
                url: URL + 'appstores/checkGoogleAccount',
                type: 'POST',
                data: data,
                success: function (response) {
                    if (response == "1") {
                        $(".worning-icon").attr("src", "/admin/img/ok_w_shadow.png");
                        $(".developerAccount").unbind("click");
                        $(".developerAccount").removeClass("fourth-btn");
                        $(".developerAccount").removeClass("developerAccount");
                    }
                }
            });
        }
    });
}
