
$(document).ready(function () {

    $('.clickSearch').on('click', function () {
        
        qs = $.trim($("#qs").val());
        runSearch(qs);        
    });

    $('#qs').keyup(function (e) {
        var code = e.which;
        if (code == 13) {
            e.preventDefault();
            qs = $.trim($("#qs").val());
            runSearch(qs);
        }        
    });

    callback = window['init_' + $("#controler").val() + '_' + $("#action").val()];
    if (typeof callback == 'function') {
        callback();
    }

});

function runSearch(qs) {

    if (qs == '') {
        return false
    }

    window.location.href = "/mobile/apps/search/" + qs;
}



function validateNumber(v) {
    num = v.toString()
    if (num.match(/^(?:[1-9]\d*|0)?(?:\.\d+)?$/)) {
        return true;
    }
    else {
        return false;
    }
}
function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,7})?$/;
    if (!emailReg.test($email)) {
        return false;
    } else {
        return true;
    }
}
function validatePhone(txtPhone) {
    var filter = /^[0-9-+]+$/;
    if (filter.test(txtPhone)) {
        return true;
    }
    else {
        return false;
    }
}
function validateInternationalPhone (phone) {
    var regex = /^\+(?:[0-9] ?){6,14}[0-9]$/;

    if (regex.test(phone)) {
        return true;
    }
    else {
        return false;
    }
}
function validateUrl(s) {
    var res = s.match(/(http(s)?:\/\/.*)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z0-9]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    if (res == null || s.indexOf('http') != 0)
        return false;
    else
        return true;
}
function isNullOrEmpty(obj) {
    return (obj === undefined || obj === null);
}
function hexc(colorval) {
    var parts = colorval.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    delete (parts[0]);
    for (var i = 1; i <= 3; ++i) {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1) parts[i] = '0' + parts[i];
    }
    return '#' + parts.join('');
}
function rgba(hex, opacity) {
    if (typeof opacity == 'undefined') {
        opacity = 1;
    }
    hex = hex.replace('#', '');
    r = parseInt(hex.substring(0, 2), 16);
    g = parseInt(hex.substring(2, 4), 16);
    b = parseInt(hex.substring(4, 6), 16);

    result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
    return result;
}

function hexToHsl(color){
    var color = color.substring(1);      // strip #
    var rgb = parseInt(color, 16);   // convert rrggbb to decimal
    var r = (rgb >> 16) & 0xff;  // extract red
    var g = (rgb >> 8) & 0xff;  // extract green
    var b = (rgb >> 0) & 0xff;  // extract blue

    r /= 255, g /= 255, b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if (max == min) { h = s = 0; } 
    else {
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
			
        h /= 6;
    }
		
    return [(h*100+0.5)|0, ((s*100+0.5)|0) , ((l*100+0.5)|0)];
}
function getLumin(color) {
    var color = color.substring(1);      // strip #
    var rgb = parseInt(color, 16);   // convert rrggbb to decimal
    var r = (rgb >> 16) & 0xff;  // extract red
    var g = (rgb >> 8) & 0xff;  // extract green
    var b = (rgb >> 0) & 0xff;  // extract blue

    var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709

    return luma;
}
function convertMSecondsToTime(duration, scope) {
    if (typeof scope == 'undefined') {
        scope = 'minutes';
    }
    var milliseconds = parseInt((duration % 1000) / 100)
            , seconds = parseInt((duration / 1000) % 60)
            , minutes = parseInt(duration / (1000 * 60))
            , hours = parseInt(duration / (1000 * 60 * 60));

    if (scope == 'hours') {
        minutes = minutes % 60;
        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;
        return hours + ":" + minutes + ":" + seconds;
    }
    if (scope == 'minutes') {
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;
        return minutes + ":" + seconds;
    }
 }

function convertTo24Hours(time) {
    if (time.length == 5) {
        return time;
    }
    var result = '';
    var onlyTime = time.substring(0,5);
    var merid = time.substring(5);
    actTime = onlyTime.split(':');
    actTime[0] = actTime[0] == '12' ? '00' : actTime[0];
    result = actTime[0] + ":" + actTime[1];
    if (merid.toLowerCase() == 'pm') {
        var newHour = parseInt(actTime[0]) + 12;
        result = newHour + ":" + actTime[1];
    }
    return result;
}

// Cookies
function createCookie(name, value, days){
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
function eraseCookie(name) {
    createCookie(name, "", -1);
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//Scrolling
function disableOuterScroll() {
    $('.no-outer-scroll').unbind('mousewheel DOMMouseScroll');
    $('.no-outer-scroll').bind('mousewheel DOMMouseScroll', function (e) {
        var e0 = e.originalEvent, delta = e0.wheelDelta || -e0.detail;

        this.scrollTop += (delta < 0 ? 1 : -1) * 30;
        e.preventDefault();
    });
}


var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

(function ($) {
    $.fn.countTo = function (options) {
        // merge the default plugin settings with the custom options
        options = $.extend({}, $.fn.countTo.defaults, options || {});

        // how many times to update the value, and how much to increment the value on each update
        var loops = Math.ceil(options.speed / options.refreshInterval),
            increment = (options.to - options.from) / loops;

        return $(this).each(function () {
            var _this = this,
                loopCount = 0,
                value = options.from,
                interval = setInterval(updateTimer, options.refreshInterval);

            function updateTimer() {
                value += increment;
                loopCount++;
                $(_this).html(value.toFixed(options.decimals));

                if (typeof (options.onUpdate) == 'function') {
                    options.onUpdate.call(_this, value);
                }

                if (loopCount >= loops) {
                    clearInterval(interval);
                    value = options.to;

                    if (typeof (options.onComplete) == 'function') {
                        options.onComplete.call(_this, value);
                    }
                }
            }
        });
    };

    $.fn.countTo.defaults = {
        from: 0,  // the number the element should start at
        to: 100,  // the number the element should end at
        speed: 1000,  // how long it should take to count between the target numbers
        refreshInterval: 100,  // how often the element should be updated
        decimals: 0,  // the number of decimal places to show
        onUpdate: null,  // callback method for every time the element is updated,
        onComplete: null,  // callback method for when the element finishes updating
    };

    //Scrolling for the window
    $.fn.smartScroller = function () {
        var elem = $(this);
        var frame = $.isWindow(elem[0]) ? $(document) : elem.parent();
        $(this).data('scrolltop', $(this).scrollTop());
        elem.scroll(function () {
            var direction = $(this).data('scrolltop') < $(this).scrollTop() ? 'down' : 'up';
            var scrollpos = elem.height() + $(this).scrollTop();
            var elemHeight = frame.height();
            $(this).data('scrolltop', $(this).scrollTop());
            var percent = 0;
            if ((frame.height() - elem.height()) > 0) {
                percent = ($(this).scrollTop() / (frame.height() - elem.height())) * 100;
            }

            elem.trigger('smartScroll', {
                direction: direction,
                percent: percent
            });

        });
    }

    $.fn.bbInnerScroller = function (options,args) {

        var elem = $(this);

        var defaultOptions = {
            scrollURL: elem.data('scrollurl'),
            scrollItem: elem.data('scrollitem'),
            scrollFunc: elem.data('scrollfunc'),
            scrollSearch: elem.data('scrollsearch'),
            scrollAjaxData: typeof (elem.data('scrollajaxdata')) != 'undefined' ? elem.data('scrollajaxdata') : {},
            initOnStart: false
        }

        var bbScroller = function (elem, options) {
            this.element = $(elem);
            this.options = options;
            this.element.data('bbscroller', this);
           
            this.addScroll();
            if (typeof (options.scrollURL) !== 'undefined') {
                this.addOnScrollURL(options.scrollURL, options.scrollItem, options.scrollSearch, options.scrollAjaxData);
            }
            
            
        }

        bbScroller.prototype = {
            addScroll: function () {
                if (!this.element.hasClass('bbscrolling')) {
                    this.element.addClass('bbscrolling');
                    this.element.customScrollbar({
                        skin: "paptap-skin",
                        hScroll: false,
                        updateOnWindowResize: true,
                        preventDefaultScroll: true
                    });
                }
            },
            addOnScrollURL: function (url,itemSelector,searchSelector,scrollAjaxData) {
                this.element.on('customScroll', function (event, scrollData) {
                    var bbs = $(this).data('bbscroller');
                    if (bbs.element.hasClass('processingScroll') || bbs.element.hasClass('noMoreScroll')) {
                        return;
                    }

                    if (scrollData.direction != 'down' || scrollData.scrollPercent < 75) {
                        return;
                    }

                    bbs.element.addClass('processingScroll');

                    var itemCount = 0;
                    if (typeof (itemSelector) === 'undefined') {
                        itemCount = bbs.element.find('.overview > *').length;
                    }
                    else {
                        itemCount = $(itemSelector).length;
                    }

                    var search = '';
                    if (typeof (searchSelector) !== 'undefined') {
                        search = $(searchSelector).val();
                    }

                    var bbscrollObj = bbs;
                    var data = { existingCount: itemCount, searchkey: search };
                    data = $.extend(data, scrollAjaxData);
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        success: function (data) {
                            bbscrollObj.element.removeClass('processingScroll');
                            if (data == '') {
                                bbscrollObj.element.addClass('noMoreScroll');
                            }
                            else {
                                bbscrollObj.element.find('.overview').append(data);
                                bbscrollObj.element.customScrollbar('resize', true);
                            }
                        }
                    });

                });
            },
            addOnScrollFunction:function(){
                this.element.on('customScroll', function (event, scrollData) {
                    var bbs = $(this).data('bbscroller');

                    var scrollFunc = bbs.options.scrollFunc;

                    if ($.isFunction(scrollFunc)) {
                        scrollFunc(scrollData);
                    }
                });
            },
            appendHTML: function (html) {
                this.element.find('.overview').append(html);
                this.element.customScrollbar('resize', true);
            },
            prependHTML: function (html) {
                this.element.find('.overview').prepend(html);
                this.element.customScrollbar('resize', true);
            },
            replaceHTML: function (html) {
                this.element.find('.overview').html(html);
                this.element.removeClass('noMoreScroll');
                this.element.customScrollbar('resize', true);
                if (html == '') {
                    this.element.addClass('empty-list');
                }
                else {
                    this.element.removeClass('empty-list');
                }
            },
            refreshScroll: function () {
                this.element.customScrollbar('resize', true);
            },
            scrollToBottom: function () {
                var height = this.element.height();
                this.element.customScrollbar('scrollToY', height);
            },
            resetElement: function () {
                
                this.element.removeClass('noMoreScroll');
                this.element.addClass('processingScroll');
                var searchSelector = this.options.scrollSearch;
                var scrollAjaxData = this.options.scrollAjaxData;
                var url = this.options.scrollURL;
                var itemCount = 0;
                

                var search = '';
                if (typeof (searchSelector) !== 'undefined') {
                    search = $(searchSelector).val();
                }

                var bbscrollObj = this;
                var data = { existingCount: itemCount, searchkey: search };
                this.element.ajaxloader('show');
                data = $.extend(data, scrollAjaxData);
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        bbscrollObj.element.ajaxloader('hide');
                        bbscrollObj.element.removeClass('processingScroll');
                        if (data == '') {
                            bbscrollObj.element.addClass('noMoreScroll');
                        }
                        else {
                            bbscrollObj.element.find('.overview').html(data);
                            
                        }
                        bbscrollObj.element.customScrollbar('resize', true);
                        bbscrollObj.element.customScrollbar('scrollToY', 0);
                        
                       
                    }
                });
            }
        }

        return this.each(function () {
            if (options == undefined)
                options = defaultOptions;
            if (typeof (options) == "string") {
                var bbscroller = $(this).data("bbscroller");
                if (bbscroller)
                    if ($.isFunction(bbscroller[options])) {
                        bbscroller[options](args);
                    }
                    else {
                        throw "Invalid function";
                    }
            }
            else if (typeof (options) == "object") {
                options = $.extend(defaultOptions, options);
                new bbScroller($(this), options);
            }
            else
                throw "Invalid type of options";
        });


    }


    /*
    Delays search inputs to avoid confusion
    callback - Function to run after delay
    delay - delay time in ms
    runNow - function to run when the key is pressed (useful to show waiting states)
    */
    $.fn.timedSearch = function (callback, delay,runNow) {
        var typingTimer;
        if (typeof (delay) === 'undefined') {
            delay = 1000;
        }
        $(this).on('keyup', function (e) {
            if ($.isFunction(runNow)) {
                runNow();
            }
            var word = $(this).val();
            clearTimeout(typingTimer);
            
            if (e.keyCode == 13) {
                callback(word);

            }
            else {
                typingTimer = setTimeout(function () {
                    callback(word);

                }, delay);
            }


        });
    }

    $.fn.windowSmartScroller = function(options){
        var elem = $(this);
        var scrollFunction = function(e,scrollData){
            if ($(elem).length == 0) {
                $(window).off('smartScroll', scrollFunction);
                return;
            }

            if (scrollData.direction != 'down' || scrollData.percent < 70 || $(elem).hasClass('scrolling') || $(elem).hasClass('noMoreEntries') || $(elem).is(':hidden')) {
                return;
            }

            $(elem).addClass('scrolling');

            var callback = function () {
                $(elem).removeClass('scrolling');
            }

            options.onScroll(callback);
        }

        $(window).on('smartScroll', scrollFunction);
    }
})(jQuery);


function ptAlert(options) {

    var settings = $.extend({
        header: "Error",
        message: "Oops Something went wrong.",
        btnOK: "Close",
        width: 500,
        height: 300,
        type: 'error',
        noX: false
    }, options);

    var icon = '/mobile/img/error.png';
    switch (settings.type) {
        case 'error':
            break;
        case 'confirmed':
            icon = '/mobile/img/ok_hand.png';
            break;
        case 'warning':
            icon = '/mobile/img/atten_w_shadow.png';
            break;
        default:
            icon = '';
            break;
    }
    var iconElem = icon != '' ? '<img src="' + icon + '"/>' : '';
    var html = '<div class="system-message"><div class="title alert">' + iconElem + '<h1>#title#</h1></div><div class="popover-body">#message#</div><div class="popover-buttons"><div class="popover-confirm popover-btn second-btn"><span>#btnlabel#</span></div></div></div>';
    html = html.replace('#title#', settings.header).replace('#message#', settings.message).replace('#btnlabel#', settings.btnOK);

    $.popover({
        type: 'alert',
        width: settings.width,
        height: settings.height,
        closebutton: '.popover-confirm',
        html: html,
        blockui: true,
        noX: settings.noX,
        onOpen: function () {
            $(document).off('click', '.popover-confirm');
            $(document).on('click', '.popover-confirm', function () {
                if ($.isFunction(settings.onOk)) {
                    settings.onOk();
                }
                //$.popover.close();

            });
        }
    });

}
