﻿/// <reference path="global.js" />

$(document).ready(function () {

    $('.loadReviews').click(function () {

        totalReviews = $(this).data("total_reviews");

        var data = {
            bizId: $(this).data("id"),
            limitStart: $(".reviewsContainer").children().length,
        };

        $.ajax({
            type: 'POST',
            url: '/mobile/app/getReviews',
            data: data,
            success: function (data) {
                $('.reviewsContainer').append(data);
                if ($(".reviewsContainer").children().length >= totalReviews) {
                    $('.loadReviews').hide();
                }
            }
        });
    });

    $('button#rate').on('click', function () {
        $('.rate-page').addClass('open');
        $('.shadow').addClass('open');
    });

    $('.close-icon').on('click', function () {
        closeRatingWindow();
    });

    $('.submitButton').on('click', function () {
        
        $('.comment.error').hide();

        if ($(this).data("score") == 0) {
            $('.rateErrorText').html(language.rate_score_error);
            $('.comment.error').show();
            return false;
        }

        if ($.trim($('#visitorsName').val()) == "") {
            $('.rateErrorText').html(language.rate_name_error);
            $('.comment.error').show();
            return false; 
        }

        var data = {
                bizId: $(this).data("id"),
                score: $(this).data("score"),
                visitorsName: $.trim($('#visitorsName').val()),
                visitorsComments: $.trim($('#visitorsComments').val())
        };

        $.ajax({
            type: 'POST',
            url: '/mobile/app/setRating',
            data: data,
            success: function (data) {
                createCookie("rating", 1, "");
                closeRatingWindow();
                window.location.reload();
            }
        });

    });

    $('.ratings .fa').on('click', function () {
        selectedScore = $(this).data("score");
        $('.ratings .fa').removeClass("fa-star");
        $('.ratings .fa').addClass("fa-star-o");
        changeRateLabel(selectedScore);
        $('.submitButton').data("score", selectedScore);

        for (i = 1; i <= selectedScore; i++) {
            $('.ratings .star' + i).addClass("fa-star");
            $('.ratings .star' + i).removeClass("fa-star-o");
        }
    });

});

function closeRatingWindow() {
    $('.rate-page').removeClass('open');
    $('.shadow').removeClass('open');
}

function changeRateLabel(score) {
    switch (score) {
        case 1:
            $('.rateLabel').html("Poor");
            break;
        case 2:
            $('.rateLabel').html("Fair");
            break;
        case 3:
            $('.rateLabel').html("Good");
            break;
        case 4:
            $('.rateLabel').html("Very Good");
            break;
        case 5:
            $('.rateLabel').html("Excellent");
            break;
    }
}


function init_app_overview() {
    $('.startDownload').on('click', function () {
        $(".iosDevWrapper").fadeToggle();
    });

    $('.openOnMobile').on('click', function () {
        ptAlert(
            {
                message: language.use_in_mobile
            }
        );
    });

    $('.closeHelp').on('click', function () {
        $(".iosDevWrapper").fadeToggle();
    });

    $(".download-wrapper").ajaxloader("show");

}

