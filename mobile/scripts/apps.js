/// <reference path="global.js" />
$(document).ready(function () {

    // TAB SCRIPTS

    $('.tab-menu ul li').eq(0).addClass('active');
    $('.tab-content').eq(0).addClass('active');

    $('.tab-menu ul li').click(function () {

        var tab_id = $(this).attr('data-tab');

        $('.tab-menu ul li').removeClass('active');
        $('.tab-content').removeClass('active');

        $(this).addClass('active');
        $("#" + tab_id).addClass('active');

    })

    // EDITOR CHOICE SLIDER

    $(".editor-slider").owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        autoplay: false,
        loop: false,
        margin: 10,
        stagePadding: 40,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 0,
                stagePadding: 30,
            },
            480: {
                items: 1,
            }
        }
    });


    // TOP RANKED SLIDER

    $(".top-ranked-slider").owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        autoplay: false,
        loop: false,
        margin: 10,
        stagePadding: 40,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 0,
                stagePadding: 30,
            },
            480: {
                items: 1,
            }
        }

    });


    // TOP RATED SLIDER

    $(".top-rated-slider").owlCarousel({
        items: 3,
        nav: false,
        dots: true,
        autoplay: false,
        loop: false,
        margin: 10,
        stagePadding: 40,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 0,
                stagePadding: 30,
            },
            480: {
                items: 2,
            }
        }

    });


    // TOP DEALS SLIDER

    $(".top-deals-slider").owlCarousel({
        items: 1,
        nav: false,
        dots: true,
        autoplay: false,
        loop: false,
        margin: 10,
        stagePadding: 40,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 0,
                stagePadding: 30,
            },
            480: {
                items: 1,
            }
        }
    });

    // ACTIONS
    $('.openBiz').on('click', function () {
        bid = $(this).data("id");
        window.location.href = "https://market.bobile.com/mobile/app/overview/" + bid;
    });

});


function init_apps_home() {
  
}

function init_apps_categories(){

}
