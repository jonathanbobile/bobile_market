<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=0.6">   
        <link rel="icon" href="<?=$this->data->biz_icon?>">

        <title><?=$this->data->biz_short_name?></title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> 
        <link  href="/mobile/styles/font-awesome.min.css" rel="stylesheet">

        <?
            require_once(ROOT_PATH.VIEWS_PATH.'_templates/global_css.php');
        ?>

        <!-- JS -->
        <?
            require_once(ROOT_PATH.VIEWS_PATH.'_templates/global_js.php');
        ?>
        <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5ba540e51b076c0011d5412b&product=custom-share-buttons"></script>

        <script type="text/javascript">

            var result = <?=  "'".str_replace('\"','\\\"',str_replace("'","\'",json_encode($this->lang))) . "'"?>;
            var language = JSON.parse(result);

        </script>
    </head>
    <body>
        <div class="container">

            <!-- HEADER START -->
            <header class="site-header">
                <img src="<?=$this->data->biz_logo?>" alt="Banner">
            </header>
            <!-- HEADER END -->

            <!-- GENERAL INFO -->
            <div class="row general-info-wrp">
                <div class="col-md-6 col-xs-8">
                    <div class="logo">
                        <img src="<?=$this->data->biz_icon?>" alt="Logo">
                        <div class="general-info">
                            <h3><?=$this->data->biz_short_name?></h3>
                            <span><?=$this->data->sub_cat_name?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-4">
                    <div class="share text-right">
                        <!--<div class="sharethis-inline-share-buttons"></div>-->
                        <div class="st-custom-button" data-network="sharethis">
                            <img src="<?= (isset($this->data->resellerData) && $this->data->resellerData->reseller_id > 0) ? "/mobile/image/paint_image/app/share/".str_replace("#","",$this->data->resellerData->reseller_wl_color_1) : "/mobile/img/app/share.png" ?>" alt="Share">
                        </div>
                    </div>
                </div>
            </div> 
            <!-- GENERAL INFO END -->

            <!-- BUTTON -->
            <?

            if($this->data->has_ipa == 1 || $this->data->has_apk == 1){
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="button text-right">
                        <?
                if($this->isDevice){
                    if($this->isIOS && $this->data->has_ipa == 1){
                        $folder = $this->data->apl_build_version != "" ? "/".$this->data->apl_build_version : "";
                        ?>
                            <a class="site-btn open startDownload" href="itms-services://?action=download-manifest&url=itms-services://?action=download-manifest&url=https://storage.googleapis.com/bobile/download/ipa/<?=$this->ipaId?><?=$folder?>/manifest.plist" id="bb<?=$this->ipaId?>"><?=$this->lang["download"]?></a>
                        <?
                    }
                    if(!$this->isIOS && $this->data->has_apk == 1){
                            ?>
                                <a href="<?=$this->data->biz_apk_url?>" class="site-btn open" download><?=$this->lang["download"]?></a>
                            <?
                    }
                }else{
                    ?>
                        <a href="#" class="site-btn open openOnMobile"><?=$this->lang["download"]?></a>
                    <?
                }
                        ?>
                    </div>
                </div>
            </div>
            <?
            }
            ?>
            <!-- BUTTON END -->

            <!-- STATES -->
            <div class="row states">
                <div class="col-md-4 col-xs-4">
                    <div class="state-box">
                        <img src="<?= (isset($this->data->resellerData) && $this->data->resellerData->reseller_id > 0) ? "/mobile/image/paint_image/app/download/".str_replace("#","",$this->data->resellerData->reseller_wl_color_1) : "/mobile/img/app/download.png" ?>" alt="Download">
                        <span class="count"><?=$this->data->total_downloads?></span>
                        <span><?=$this->lang["downloads"]?></span>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4">
                    <div class="state-box">
                        <img src="<?= (isset($this->data->resellerData) && $this->data->resellerData->reseller_id > 0) ? "/mobile/image/paint_image/app/thumbsup/".str_replace("#","",$this->data->resellerData->reseller_wl_color_1) : "/mobile/img/app/thumbsup.png" ?>" alt="Thumbsup">
                        <span class="count"><?=$this->data->total_ratings?></span>
                        <span><?=$this->lang["reviews"]?></span>
                    </div>
                </div>
                <div class="col-md-4 col-xs-4">
                    <div class="state-box">
                        <img src="<?= (isset($this->data->resellerData) && $this->data->resellerData->reseller_id > 0) ? "/mobile/image/paint_image/app/business/".str_replace("#","",$this->data->resellerData->reseller_wl_color_1) : "/mobile/img/app/business.png" ?>" alt="Thumbsup">
                        <span>&nbsp;</span>
                        <span><?=$this->data->sub_cat_name?></span>
                    </div>
                </div>

                <div class="col-md-12 col-xs-12">
                    <div class="border"></div>
                </div>
            </div>
            <!-- STATES END -->

            <!-- DESCRIPTION -->
            <div class="row">

                <div class="general-info description">
                    <span><?= str_replace("&nbsp;"," ", nl2br($this->data->app_description)) ?></span>
                </div>
                <div class="col-md-12 col-xs-12">
                    <div class="border"></div>
                </div>
            </div>
            <!-- DESCRIPTION -->

            <!-- APP RATING -->
            <div class="row  app-rating">
                <div class="col-md-12">
                   <h2><?=$this->lang["app_rating"]?></h2>
                </div>
                <div class="col-md-6 col-xs-4">
                    <h1 class="overall-rating"><?=$this->data->average_score?></h1>
                </div>
                <?
                $maxRate = 0;
                $rates[1] = 0;
                $rates[2] = 0;
                $rates[3] = 0;
                $rates[4] = 0;
                $rates[5] = 0;
                foreach ($this->data->rates as $rate){
                    if($rate->count > $maxRate){
                        $maxRate = $rate->count;
                    }
                }

                foreach ($this->data->rates as $rate){
                    $currentPercent = $rate->count / ($maxRate / 100);
                    $rates[$rate->stars] = 0.8 * $currentPercent;
                }
                
                ?>
                <div class="col-md-6 col-xs-8 paddingleft0">
                    <div class="rating-count-box">
                        <span class="star"><i class="fa fa-star"></i></span>
                        <span class="count">5</span>
                        <span class="progress-5 rating-progres" style="width:<?=$rates[5]?>%;"></span>
                    </div>

                    <div class="rating-count-box">
                        <span class="star"><i class="fa fa-star"></i></span>
                        <span class="count">4</span>
                        <span class="progress-4 rating-progres" style="opacity: 0.8; width:<?=$rates[4]?>%;"></span>
                    </div>

                    <div class="rating-count-box">
                        <span class="star"><i class="fa fa-star"></i></span>
                        <span class="count">3</span>
                        <span class="progress-3 rating-progres" style="opacity: 0.6; width:<?=$rates[3]?>%;"></span>
                    </div>

                    <div class="rating-count-box">
                        <span class="star"><i class="fa fa-star"></i></span>
                        <span class="count">2</span>
                        <span class="progress-2 rating-progres" style="opacity: 0.4; width:<?=$rates[2]?>%;"></span>
                    </div>

                    <div class="rating-count-box">
                        <span class="star"><i class="fa fa-star"></i></span>
                        <span class="count">1</span>
                        <span class="progress-1 rating-progres" style="opacity: 0.2; width:<?=$rates[1]?>%;"></span>
                    </div>
                    <?
                    if(!$this->hasRating){
                    ?>
                        <button id="rate"><?=$this->lang["rate"]?></button>
                    <?
                    }
                    ?>
                </div>
                <div class="col-md-12 col-xs-12">
                    <div class="border"></div>
                </div>
            </div>
            <!-- APP RATING END -->
            
            <!-- RATE PAGE -->
            <div class="rate-page">
                <div class="rate-close text-right">
                    <div class="close-icon">
                        <img src="/mobile/img/app/close.png" alt="">
                    </div>
                </div>
                <h2><?=  str_replace("#app_name#", $this->data->biz_short_name,$this->lang["rate_app_name"])?></h2>
                <div class="ratings text-center">
                    <i class="fa fa-star-o star1" data-score="1"></i>
                    <i class="fa fa-star-o star2" data-score="2"></i>
                    <i class="fa fa-star-o star3" data-score="3"></i>
                    <i class="fa fa-star-o star4" data-score="4"></i>
                    <i class="fa fa-star-o star5" data-score="5"></i>
                    <div class="comment">
                        <p class="rateLabel"><?=$this->lang["rate_score"]?></p>
                    </div>
                </div>
                <div class="users-info">
                   
                    <div class="field">
                        <label for=""><?=$this->lang["rate_name"]?></label>
                        <input id="visitorsName" type="text">
                    </div>
                    <div class="field">
                        <label for=""><?=$this->lang["rate_comments"]?></label>
                        <textarea id="visitorsComments"></textarea>
                    </div>
                    <div class="comment error">
                        <p class="rateErrorText"></p>
                    </div>
                    <div class="field submit">
                        <input type="submit" class="submitButton" value="<?=$this->lang["rate"]?>" data-score="0" data-id="<?=$this->bizId?>">
                    </div>
                  
                </div>
            </div>
            <!-- RATE PAGE -->

            <div class="shadow"></div>


            <!-- REVIEWS -->
            <div class="row feedback-wrap">
                <div class="col-md-12 reviewsContainer">
                    <?
                    foreach ($this->data->reviews as $review)
                    {
                    ?>
                        <div class="review-box">
                            <h2><?= $review->rate_nickname == "" ? "Guest $review->rate_id" : $review->rate_nickname ?></h2>
                            <div class="feedback">
                                <i class="fa fa-star<?= $review->rate_score < 1 ? "-o" : "" ?>"></i>
                                <i class="fa fa-star<?= $review->rate_score < 2 ? "-o" : "" ?>"></i>
                                <i class="fa fa-star<?= $review->rate_score < 3 ? "-o" : "" ?>"></i>
                                <i class="fa fa-star<?= $review->rate_score < 4 ? "-o" : "" ?>"></i>
                                <i class="fa fa-star<?= $review->rate_score < 5 ? "-o" : "" ?>"></i>
                                <span><?= utilityManager::getFormattedTime("",false,$review->rate_date_time)?></span>
                            </div>

                            <div class="comment">
                                <p><?=$review->rate_review_text?></p>
                            </div>
                        </div>
                    <?
                    }
                    
                    ?>
                    
                </div>

                <div class="col-md-12 <?= $this->data->total_ratings <=3 ? "hidden-tag" :"" ?>">
                    <div class="more-btn text-center loadReviews" data-id="<?=$this->bizId?>" data-total_reviews="<?=$this->data->total_ratings?>">
                        <button><?=$this->lang["more_reviews"]?></button>
                    </div>
                </div>
            </div>
            <!-- REVIEWS END -->
        
            <!-- FOOTER START -->
            <? include("views/_templates/footer.php")?>
            <!-- FOOTER END -->

            <div class="ios-dev-help iosDevWrapper">

                <div class="row">
                    <div class="col-md-12">
                        <img src="/mobile/img/app/x.png" class="help-close closeHelp" alt="close">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="download-wrapper"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 downloading-text">
                        <?=$this->lang["downloading"]?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 help-header">
                        <?=$this->lang["ios_description1"]?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 help-sub-text">
                        <?=$this->lang["ios_description2"]?>
                    </div>
                </div>

                <div class="row help-section">
                    <div class="col-md-12 help-text">
                        <div class="float-left help-section-number">1</div>
                        <div class="help-section-text"><?=$this->lang["ios_description3"]?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <img src="/mobile/img/app/img-1.png" class="help-section-image" alt="img1">
                    </div>
                </div>

                <div class="row help-section">
                    <div class="col-md-12 help-text">
                        <div class="float-left help-section-number">2</div>
                        <div class="help-section-text"><?=$this->lang["ios_description4"]?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <img src="/mobile/img/app/img-2.png" class="help-section-image" alt="img2">
                    </div>
                </div>

                <div class="row help-section">
                    <div class="col-md-12 help-text">
                        <div class="float-left help-section-number">3</div>
                        <div class="help-section-text"><?=$this->lang["ios_description5"]?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <img src="/mobile/img/app/img-3.png" class="help-section-image" alt="img3">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="ok-wrapper">
                        <img src="/mobile/img/app/green-v.png" alt="img4">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 help-header">
                        <?=$this->lang["ios_description6"]?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 help-sub-text footer">
                        <?=$this->lang["ios_description7"]?>
                    </div>
                </div>

            </div>

        </div>

    <!-- NECESSARY SCRIPTS -->

    </body>
</html>



