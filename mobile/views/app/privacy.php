<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=0.6">   
        <link rel="icon" href="<?=$this->data->biz_icon?>">

        <title><?=$this->data->biz_short_name?> - Privacy</title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> 
        <link  href="/mobile/styles/font-awesome.min.css" rel="stylesheet">

        <?
            require_once(ROOT_PATH.VIEWS_PATH.'_templates/global_css.php');
        ?>

        <!-- JS -->
        <?
            require_once(ROOT_PATH.VIEWS_PATH.'_templates/global_js.php');
        ?>
        
    </head>
    <body>
        <div class="container">

            <!-- GENERAL INFO -->
            <div class="row general-info-wrp">
                <div class="col-md-12">
                    <div class="logo">
                        <a href="/mobile/app/overview/<?=$this->bizId?>"><img src="<?=$this->data->biz_icon?>" alt="Logo"></a>
                        <div class="general-info">
                            <h3><?=$this->data->biz_short_name?></h3>
                        </div>
                    </div>
                </div>
            </div> 
            <!-- GENERAL INFO END -->

            <!-- DESCRIPTION -->
            <div class="row">

                <div class="general-info privacy">

                    <h1><?=$this->lang["footer_privacy_policy"]?></h1>

                    <p><strong><span class="header">Welcome to <?=$this->data->biz_short_name?> Privacy  Policy!</span></strong> <br>
                      <?=$this->data->biz_short_name?><strong> </strong>cares deeply  about the privacy of its users and is fully committed to protect their personal  information and use it properly in compliance with data privacy laws. This  policy describes how we may collect and use personal information, and the  rights and choices available to our users regarding such information.<br><br>
                      We strongly urge you to read this  policy and make sure you fully understand it, before you access or use any of  our services. 

                    </p>
                    <p><strong><span class="header">1. Please read carefully!</span></strong><br>
                      This <?=$this->data->biz_short_name?> Privacy Policy  ("Privacy Policy") describes how we (<?=$this->data->biz_short_name?>, "we" or "us") collect  and use information pertaining to each of our registered users ("User" ,"You",  "App Users in connection with their access to and use of <?=$this->data->biz_short_name?>'s  mobile app.<br><br>
                      The purpose of this Privacy Policy is  to provide you with a clear explanation of when, why and how we collect and use  your personal information, as well as an explanation of your statutory  rights. This Privacy Policy is not intended to override the terms of any  contract you have with us, nor any rights you might have under applicable data  privacy laws.<br><br>
                      Read this policy and make sure you  fully understand our practices in relation to your Personal Information, before  you access or use any of our Services.  If you read and fully understand  this Privacy Policy, and remain opposed to our practices, you must  immediately avoid or discontinue the use of our app.  If you have any  questions or concerns regarding this policy, please contact us.<br><br>
                      By accessing or using any of our  services, you acknowledge that you have read this Privacy Policy.<br>
                      </p>
                    <p>
                        <strong><span class="header">2. What information do we  collect?</span></strong><br>
                      We collect Individually identifiable  information, namely information that identifies an individual or may with  reasonable efforts cause the identification of an individual or may be of  private or sensitive nature ("Personal Information"). The Personal Information  collected by us mainly consists of contact details (e.g., e-mail address or  phone number) and billing details (name, physical billing address, payment  method, and transaction details). Only required Users' information is collected  for the purpose of connection, identification, and payment.<br><br>
                      For the avoidance of doubt, any  Non-Personal Information that is connected or linked to Personal Information  (for example, in order to improve the Services we offer) is deemed and treated  by us as Personal Information, as long as such connection or linkage exists.<br>
                        </p>
                    <p>
                      <strong><span class="header">3. How do we collect such  information?</span></strong><br>
                      There are two main methods we use:
                    <ol start="1" type="1">
                      <li>We collect       information through the use of the app. When a User registers and uses the       App, we are aware of it and will usually gather, collect and record such       uses, sessions and related information.</li>
                      <li>We collect       information which Users provide us voluntarily. For example, we collect       the Personal Information you provide us when you register to our app.</li>
                    </ol>
                    </p>
                    <p><strong><span class="header">4. Why do we collect such  information?</span></strong><br>
                      We collect Information for the  following purposes:
                    <ol start="1" type="1">
                      <li>To provide and       operate the app;</li>
                      <li>To further       develop, customize and improve our Services, based on Users' common or       personal preferences, experiences and difficulties;</li>
                      <li>To provide our       Users with ongoing customer assistance and technical support;</li>
                      <li>To be able to       contact our Users with general or personalized service-related       notices and promotional messages (as further detailed in <u>Section </u><span dir="RTL"> </span><span dir="RTL"> </span><u><span dir="RTL"><span dir="RTL"> </span><span dir="RTL"> </span>7</span></u><span dir="LTR"> </span><span dir="LTR"> </span><span dir="LTR"> </span><span dir="LTR"> </span> below);<strong></strong></li>
                      <li>To enhance our       data security and fraud prevention capabilities;</li>
                      <li>To comply with       any applicable laws and regulations.</li>
                    </ol>
                        <br />
                    We will only use your Personal  Information for the purposes set out in Section 4 where we are satisfied that:<br /><br />

                    <ol start="1" type="1">
                      <li>Our use of your       Personal Information is necessary to perform a contract or take steps to       enter into a contract with you (e.g. to provide you with our Services, to       provide you with our customer assistance and technical support), or</li>
                      <li>Our use of your       Personal Information is necessary to comply with a relevant legal or       regulatory obligation that we have, or</li>
                      <li>Our use of your       Personal Information is necessary to support legitimate interests that we       have as a business (for example, to maintain and improve our Services by       identifying user trends and the effectiveness of marketing campaigns and       identifying technical issues), provided it is conducted at all times in a       way that is proportionate, and that respects your privacy rights.</li>
                    </ol><br />
                    No one under age 18 should provide any  Personal Information to us through any of our Services.  We do not  knowingly collect Personal Information from children under 18. Parents and  guardians should supervise their children's activities at all times.<br><br />
                      We also collect and use information in  order to contact our users, and in order to comply with the laws applicable to  us.<br>
                     </p>
                    <p>
                        <strong><span class="header">5. Where do we store your  information?</span></strong><br>
                      <?=$this->data->biz_short_name?>'s Users' Personal Information may  be maintained, processed and stored by <?=$this->data->biz_short_name?> and service providers  in the United States of America, in Europe (including in Lithuania, Germany and  Ukraine), in Israel, and in other jurisdictions as necessary for the proper delivery  of our Services and/or as may be required by law (as further explained below).<br><br>
                      <?=$this->data->biz_short_name?>'s service providers  that store or process your Personal Information on <?=$this->data->biz_short_name?>'s behalf  are each contractually committed to keep it protected and secured, in  accordance with industry standards and regardless of any lesser legal  requirements which may apply in their jurisdiction.<br><br>
                      Upon request, <?=$this->data->biz_short_name?>will  provide you with information about whether we hold any of your Personal  Information. You may access, correct or update your personal information  at all times by yourself.<br><br>
                      We will respond to your request via  returned email within a timeframe imposed by local laws or a reasonable  timeframe. Only once you receive a written confirmation from <?=$this->data->biz_short_name?>regarding  your request, you can regard your data as deleted.<br><br>
                      After completing this process, you can  no longer use any of your <?=$this->data->biz_short_name?>Services, your User Account and all  its data will be removed permanently, and <?=$this->data->biz_short_name?>will not be able to  restore your account or retrieve your data in the future. If you contact <?=$this->data->biz_short_name?>in  the future, the system will not recognize your account and Support agents will  not be able to locate the deleted account.<br><br>
                      <?=$this->data->biz_short_name?>'s data storage  providers are contractually committed to protect and secure your data.<br><br>
                      Among other things, <?=$this->data->biz_short_name?>adheres  to the EU - US & to the Swiss-US Privacy Shield Principles, for  further protecting and enhancing our user's privacy.<br><br>
                      Data Localization Obligations: If  you reside in a jurisdiction that imposes "data localization" or "data  residency" obligations (i.e., requiring that Personal Information of its  residents be kept within the territorial boundaries of such jurisdiction), and  this fact comes to our attention, we may maintain your Personal Information  within such territorial boundaries, if we are legally obligated to do so.<br><br>
                      You acknowledge that while doing so, we  may continue to collect, store and use your Personal Information elsewhere,  including in the United States of America as explained above.

                    </p>
                    <p><strong><span class="header">6. Sharing personal  information with third parties</span></strong> <br>
                      <?=$this->data->biz_short_name?>will not share your  Personal Information with third parties (or otherwise allow them access to it)  except for and only in the following manners and instances:<br><br>
                      <strong>6.1 Law Enforcement,  Legal Requests, and Duties:</strong> <br>
                      Where permitted by local data  protection laws, <?=$this->data->biz_short_name?>may disclose or otherwise allow others access  to your Personal Information pursuant to a legal request, such as a subpoena,  legal proceedings, search warrant or court order, or in compliance with  applicable laws, if we have good faith belief that the law requires us to  do so, with or without notice to you.<br><br>
                      <strong>6.2  Service Providers</strong><strong> </strong><br>
                      The  following third parties may have access to your Personal Information. The  reason is to perform the tasks assigned to them on our behalf. However, they  are obligated not to disclose or use the information for any other purpose.
                    <ol>
                      <li><span dir="LTR"> </span>To facilitate our  Service;</li>
                      <li><span dir="LTR"> </span>To provide the  Service on our behalf;</li>
                      <li><span dir="LTR"> </span>To perform  Service-related services; or</li>
                      <li><span dir="LTR"> </span>To assist us in  analyzing how our Service is used.</li>
                    </ol>
                        <br>
                    To clarify, <?=$this->data->biz_short_name?>may share  your Personal Information in manners other than as described above, pursuant to  your explicit approval, or if we are legally obligated to do so. Moreover, <?=$this->data->biz_short_name?>may  transfer, share, disclose or otherwise use Non-personal Information in its sole  discretion and without the need for further approval.<br>
                    </p>
                    <p>
                      <strong><span class="header">7. Communications from <?=$this->data->biz_short_name?></span></strong><br><br>
                      <strong>7.1. Promotional  Messages:</strong> <br>
                      We may use your Personal Information to  send you promotional content and messages by e-mail, text messages, direct  text messages, marketing calls and similar forms of communication from <?=$this->data->biz_short_name?>through  such means.<br><br>
                      If you do not wish to receive such  promotional messages or calls, you may notify <?=$this->data->biz_short_name?>at any time or turn  off the notifications privileges on your device.<br><br>
                      We take steps to limit the promotional  content we send you to a reasonable and proportionate level and to send you  information which we believe may be of interest or relevance to you, based on  your information.<br /><br>
                      <strong>7.2. Service and  Billing Messages:</strong> <br>
                      <?=$this->data->biz_short_name?><strong> </strong>may also  contact you with important information regarding our Services, or your use  thereof.<br><br>
                      For example, we may send you a notice  (through any of the means available to us) if a certain Service is temporarily  suspended for maintenance; send you reminders or warnings regarding upcoming or  late payments for your current or upcoming subscriptions, or notify you of material  changes in our Services.<br><br>
                      It is important that you are always  able to receive such messages. For this reason, you will not be able to  opt-out of receiving such Service and Billing Messages unless you are no longer  a <?=$this->data->biz_short_name?><strong> </strong>User (which can only be done by deleting your  account - see section 5 in this document).<br>
                        </p>
                    <p>
                      <strong><span class="header">8. Your rights in  relation to your personal information</span></strong><br>
                      It is imperative that you have  control over your Personal Information. That's why <?=$this->data->biz_short_name?><strong> </strong>has  taken the steps to enable you to access, receive a copy of, update, amend,  or limit the use of your Personal Information.<br><br>
                      Before disclosing the requested  Personal Information, we may ask you for additional information in order  to confirm your identity and for security purposes.<br><br>
                      You can delete your information only by  sending a written request to  <?=$this->data->biz_short_name?> and receiving a written confirmation. Only  once you receive a written confirmation from <?=$this->data->biz_short_name?><strong> </strong>regarding  your request, you can regard your data as deleted.<br><br>
                      After completing this process, you can  no longer use any of your <?=$this->data->biz_short_name?><strong> </strong>Services, your User Account  and all its data will be removed permanently, and <?=$this->data->biz_short_name?><strong> </strong>will  not be able to restore your account or retrieve your data in the future. <?=$this->data->biz_short_name?><br>
                      </p>
                    <p>
                        <strong><span class="header">9. Data Retention</span></strong><br>
                      We may retain your Personal Information  for as long as your User Account is not permanently deleted, as indicated in  this Privacy Policy.<br><br>
                      We may continue to retain such Personal  Information as reasonably necessary to comply with our legal obligations, to  resolve disputes regarding our Users, prevent fraud and abuse, enforce our  agreements and/or protect our legitimate interests.<br><br>
                      We maintain a data retention policy  which we apply to information in our care. Upon a personal written request will  ensure it is securely deleted.<br>
                      </p>
                    <p>
                        <strong><span class="header">10. Security</span></strong><br>
                      #biz_short_name has implemented  security measures designed to protect the Personal Information you share with  us, including physical, electronic and procedural measures. Among other things,  we offer HTTPS secure access to most areas on our Services; the transmission of  sensitive payment information (such as a credit card number) through our  designated purchase forms is protected by an industry standard SSL/TLS  encrypted connection; and we regularly maintain a PCI DSS (Payment Card  Industry Data Security Standards) certification. We also regularly monitor our  systems for possible vulnerabilities and attacks and regularly seek new ways  and Third Party Services for further enhancing the security of our Services and  protection of our User's privacy.<br><br>
                      Regardless of the measures and efforts  that are being taken by #biz_short_name, we cannot and do not guarantee the  absolute protection and security of your Personal Information.<br><br>
                      Since e-mail and instant messaging are  not recognized as secure forms of communications, we request and encourage you  not to share any Personal Information via any of these methods.<br><br>
                      If you have any questions regarding the  security of our Services, you are welcome to contact us. <br>
                      </p>
                    <p>
                        <strong><span class="header">11. Updates and  interpretation</span></strong><br>
                      We may update this Privacy Policy as  required by applicable law, and to reflect changes to our information  collection, usage, and storage practices. If we make any changes that we deem  as "material" (in our sole good faith discretion), we will notify you prior to  the change becoming effective. We encourage you to periodically review this  page for the latest information on our privacy practices.<br><br>
                      Unless stated otherwise, our most  current Privacy Policy applies to all information that we have about Users,  with respect to <?=$this->data->biz_short_name?> and other Services.<br><br>
                      Any heading, caption or section title  contained herein, and any explanation is provided only for convenience, and in  no way defines or explains any section or provision hereof, or legally binds  any of us in any way.

                    </p>
                </div>           
            </div>
            <!-- DESCRIPTION -->
        
            <!-- FOOTER START -->
            <? 
            if($this->from_mobile == ""){
                include("views/_templates/footer.php");
            }
            ?>
            <!-- FOOTER END -->
        </div>
    </body>
</html>



