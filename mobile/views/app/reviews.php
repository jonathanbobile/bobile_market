<?
foreach ($this->data as $review)
{
?>
    <div class="review-box">
        <h2><?= $review->rate_nickname == "" ? "Guest $review->rate_id" : $review->rate_nickname ?></h2>
        <div class="feedback">
            <i class="fa fa-star<?= $review->rate_score < 1 ? "-o" : "" ?>"></i>
            <i class="fa fa-star<?= $review->rate_score < 2 ? "-o" : "" ?>"></i>
            <i class="fa fa-star<?= $review->rate_score < 3 ? "-o" : "" ?>"></i>
            <i class="fa fa-star<?= $review->rate_score < 4 ? "-o" : "" ?>"></i>
            <i class="fa fa-star<?= $review->rate_score < 5 ? "-o" : "" ?>"></i>
            <span><?= utilityManager::getFormattedTime("",false,$review->rate_date_time)?></span>
        </div>

        <div class="comment">
            <p><?=$review->rate_review_text?></p>
        </div>
    </div>
<?
}

?>