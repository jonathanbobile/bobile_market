<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=0.6">   
        <title>Bobile Market</title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- CSS -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> 
        <link  href="/mobile/styles/font-awesome.min.css" rel="stylesheet">
        <link href="/mobile/styles/owl.carousel.css" rel="stylesheet">
        <?
            require_once(ROOT_PATH.VIEWS_PATH.'_templates/global_css.php');
        ?>

        <!-- JS -->        
        <?
            require_once(ROOT_PATH.VIEWS_PATH.'_templates/global_js.php');
        ?>
        <script src="/mobile/scripts/owl.carousel.js"></script>
        
    </head>
    <body class="home">
       <div class="container">
       
           <? include("search.php")?>

            <div class="col-12">
                <div class="tab-box">

                    <div class="tab-menu clearfix">
                        <ul>
                            <!-- HOME TAB MENU -->
                            <li data-tab="tab-1" class="clearfix">
                                <img class="normal-icon" src="/mobile/img/apps/home-icon.png" alt="">

                                <img class="green-icon" src="/mobile/img/apps/home-icon-green.png" alt="">  Home
                            </li>
                            <!-- HOME TAB MENU END -->

                           <!-- CATEGORY TAB MENU -->
                            <li data-tab="tab-2" class="clearfix">
                                <img class="normal-icon" src="/mobile/img/apps/category-icon.png" alt=""> 

                                <img class="green-icon" src="/mobile/img/apps/category-icon-green.png" alt="">Category
                            </li>
                            <!-- CATEGORY TAB MENU END -->
                        </ul>
                    </div>
          
                    <!-- HOME TAB CONTENT -->
                    <div class="tab-content" id="tab-1">

                        <div class="section">
                            <h3>EDITOR'S CHOICE</h3>
                            <div class="editor-slider">
                                <?
                                foreach ($this->data->editors_app as $editor_apps){
                                    $bizId = utilityManager::encryptIt($editor_apps->biz_id,true);
                                ?>
                                    <div class="e-slider-box openBiz" data-id="<?=$bizId?>">
                                    <div class="e-box-inner margin15">
                                        <img src="<?=$editor_apps->market_image?>" alt="">
                                        <div class="e-slider-content">
                                            <img src="<?=$editor_apps->biz_icon?>" alt="icon">
                                            <div class="content-inner">
                                                <h2><?=$editor_apps->biz_short_name?></h2>
                                                <p><?=$editor_apps->sub_cat_name?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?
                                }
                                
                                ?>

                            </div>
                        </div>

                        <div class="section white-bg">
                            <h3>Top Ranked:</h3>
                            <div class="top-ranked-slider">

                                <?
                                foreach ($this->data->ranked_app as $ranked_apps){
                                    $bizId = utilityManager::encryptIt($ranked_apps->biz_id,true);
                                ?>
                                    <div class="t-slider-box openBiz" data-id="<?=$bizId?>">
                                    <div class="t-box-inner margin15">
                                        <div class="top-ranked-logo">
                                            <img src="<?=$ranked_apps->biz_icon?>" alt="icon">
                                        </div>
                                        <div class="top-ranked-feedback">
                                            <i class="fa fa-star<?= $ranked_apps->user_rating < 1 ? "-o" : "" ?>"></i>
                                            <i class="fa fa-star<?= $ranked_apps->user_rating < 2 ? "-o" : "" ?>"></i>
                                            <i class="fa fa-star<?= $ranked_apps->user_rating < 3 ? "-o" : "" ?>"></i>
                                            <i class="fa fa-star<?= $ranked_apps->user_rating < 4 ? "-o" : "" ?>"></i>
                                            <i class="fa fa-star<?= $ranked_apps->user_rating < 5 ? "-o" : "" ?>"></i>
                                        </div>
                                        <h2><?=$ranked_apps->biz_short_name?></h2>
                                        <p><?=$ranked_apps->sub_cat_name?></p>
                                    </div>
                                </div>
                                <?
                                }
                                
                                ?>

                            </div>
                        </div>   

                        <div class="section white-bg">
                            <h3>Top Rated:</h3>
                            <div class="top-rated-slider">

                                <?
                                foreach ($this->data->rated_app as $rated_apps){
                                    $bizId = utilityManager::encryptIt($rated_apps->biz_id,true);
                                ?>
                                    <div class="t-slider-box openBiz" data-id="<?=$bizId?>">
                                    <div class="t-box-inner top-rated margin15">
                                        <img src="<?=$rated_apps->biz_icon?>" alt="icon">
                                        <h2><?=$rated_apps->biz_short_name?></h2>
                                        <p><?=$rated_apps->sub_cat_name?></p>
                                        <div class="top-rated-feedback">
                                            <i class="fa fa-star<?= $rated_apps->user_rating < 1 ? "-o" : "" ?>"></i>
                                            <i class="fa fa-star<?= $rated_apps->user_rating < 2 ? "-o" : "" ?>"></i>
                                            <i class="fa fa-star<?= $rated_apps->user_rating < 3 ? "-o" : "" ?>"></i>
                                            <i class="fa fa-star<?= $rated_apps->user_rating < 4 ? "-o" : "" ?>"></i>
                                            <i class="fa fa-star<?= $rated_apps->user_rating < 5 ? "-o" : "" ?>"></i>
                                        </div>
                                    </div>
                                </div>
                                <?
                                }
                                
                                ?>

                            </div>
                        </div>   


                        <div class="section white-bg">
                            <h3>Top Deals:</h3>
                            <div class="top-deals-slider">

                                <?
                                foreach ($this->data->deals as $deals){
                                    $bizId = utilityManager::encryptIt($deals->biz_id,true);
                                ?>
                                    <div class="t-slider-box openBiz" data-id="<?=$bizId?>">
                                        <div class="t-box-inner top-deals margin15">
                                            <div class="top-deals-logo">
                                                <img src="<?=$deals->dealImage?>" alt="">
                                            </div>
                                            <div class="top-deals-content">
                                                <h2><?=$deals->dealTitle?></h2>
                                                <div class="top-deals-author">
                                                    <img src="<?=$deals->biz_icon?>" alt="icon">
                                                    <h4><?=$deals->biz_short_name?></h4>
                                                    <p><?=$deals->sub_cat_name?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?
                                }
                                
                                ?>

                            </div>
                        </div>  
                    </div>
                    <!-- HOME TAB CONTENT END -->
                </div>
            </div>

            <!-- FOOTER START -->
            <? include("views/_templates/footer.php")?>
            <!-- FOOTER END -->
        </div>
    </body>
</html>



