<link href="/mobile/styles/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="/mobile/styles/bootstrap/animate.css" rel="stylesheet">
<link href="/mobile/styles/global.css" rel="stylesheet">
<link href="/mobile/styles/ajaxloader/ajaxloader.css" rel="stylesheet">
<link href="/mobile/styles/spinner.css" rel="stylesheet">
<link href="/mobile/styles/jquery.paptap.popover.css" rel="stylesheet">
<?
$newVersion = str_replace(".","",microtime(true));
foreach ($this->styles as $style) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $style;?>?v=<?= $newVersion?>" />
<?php } 

if(isset($this->data->resellerData) && $this->data->resellerData->reseller_id > 0){
?>
    <link rel="stylesheet" type="text/css" href="<?= URL.VIEWS_PATH.'_templates/whitelabel_css.php';?>?v=<?= $newVersion?>" />
<?}?>