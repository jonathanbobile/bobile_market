<?
header("Content-Type: text/css");

require_once('../../libs/utilityManager.php');

$response = utilityManager::file_get_contents_pt("https://api.bobile.com/api/app_market.php?action=getWhiteLabelData&domain={$_SERVER['HTTP_HOST']}"); 
$object = json_decode($response);

$platformColor1 = $object->reseller_wl_color_1;
$platformColor2 = $object->reseller_wl_color_2;
$platformColor3 = $object->reseller_wl_color_3;
$backclientImage = $object->reseller_wl_client_back;
$accentColor1 = $object->reseller_wl_color_4;
?>

.site-btn {
    background-color: <?= $platformColor1?>;
}

.site-btn:hover {
    color: <?= $platformColor1?>;
}

button{
    color:<?= $platformColor1?>;
}

.field.submit input{
    background-color: <?= $platformColor1?>;
}

.loader-line {
    box-shadow: inset 0 0 0 6px <?= $platformColor1?>;
}
.downloading-text {
    color: <?= $platformColor1?>;
}

i.fa {
    color: <?= $platformColor1?>;
}

.ratings i.fa {
    color: <?= $platformColor1?>;
}

.rating-progres {
    background: <?= $platformColor1?>;
}

.second-btn {
    background-color: <?= $platformColor1?>;
}