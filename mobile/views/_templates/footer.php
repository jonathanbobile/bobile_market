<!-- FOOTER START -->
<footer class="<?= (isset($this->data->resellerData) && $this->data->resellerData->reseller_id > 0) ? "hidden-tag" : ""?>">
    <div class="row">
        <div class="col-md-12">
            <div class="footer-menu">
                <ul>
                    <li><a href="https://bobile.com/<?=$this->langCode?>">bobile.com</a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/case-studies/"><?=$this->lang["footer_case_studie"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/features/"><?=$this->lang["footer_features"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/pricing/"><?=$this->lang["footer_pricing"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/business-apps/"><?=$this->lang["footer_business"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/publishers-apps/"><?=$this->lang["footer_publishers"]?></a></li>
                </ul>
            </div>
            <div class="footer-menu right">
                <ul>
                    <li><a href="/mobile/app/privacy/<?=$this->bizId?>"><?=$this->lang["footer_privacy_policy"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/terms/"><?=$this->lang["footer_terms_of_use"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/blog/"><?=$this->lang["footer_blog"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/helpcenter/"><?=$this->lang["footer_help_center"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/get-in-touch/"><?=$this->lang["footer_contact_us"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/from-the-press/"><?=$this->lang["footer_press"]?></a></li>
                    <li><a href="https://bobile.com/<?=$this->langCode?>/resellers/"><?=$this->lang["footer_partners"]?></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<input type="hidden" id="controler" value="<?=$this->controller?>" />
<input type="hidden" id="action" value="<?=$this->action?>" />
<!-- FOOTER END -->