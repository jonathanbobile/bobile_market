<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
<script src="/mobile/scripts/bootstrap/bootstrap.min.js"></script>
<script src="/mobile/scripts/ajaxloader/jquery.ajaxloader.1.4.js"></script>
<script src="/mobile/scripts/jquery.paptap.popover.js"></script>
<?php 
$newVersion = str_replace(".","",microtime(true));
foreach ($this->scripts as $script) { ?>
    <script src="<?php echo $script;?>?v=<?= $newVersion?>"></script>
<?php } ?>